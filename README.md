# README du projet

## Code source de calcul de l’IS – année 2019

### Objet
Mise en ligne du code source du calcul de l’impôt sur les sociétés – description des principes et détail de l’arborescence du dépôt.

### Descritpion
La « Calculette des Professionnels » (CAPROMAS) est une application java utilisée via des webservices exposés aux applications clientes de la DGFIP dont en particulier :
- les services de télédéclaration en mode EFI qui permettent aux usagers d’effectuer directement les déclarations et les paiements à partir de leur espace professionnel accessible depuis le site impots.gouv.fr ;
- les services de télédéclaration en mode EDI qui autorisent la transmission électronique des déclarations et de certains paiements à partir des fichiers comptables, par l’intermédiaire d’un prestataire spécialisé, le partenaire EDI, ou d’un expert-comptable.

Les offres de service correspondantes sont spécialisées par formulaire déclaratif.

Les calculs de l’impôt sur les sociétés, objet de cette publication, sont utilisés à l’occasion du dépôt des déclarations de solde d’IS, via le formulaire 2572.

La classe principale « CalculFormulaire2572Metier.java » se trouve sous « ./java/fr/gouv/impots/appli/capromas/domaine ».
 
Le flux SOAP envoyé par l’applicatif client à la calculette est constitué d’une liste de code de références valorisés numériquement. Ces codes correspondent aux données présentes dans le formulaire et sont identifiés dans le fichier xml « mdd_2572.xml » présent sous « ./resources ».

Ce dernier permet d’associer chaque code utilisé dans les sources Java au libellé de champ de saisie correspondant dans le formulaire proposé en ligne aux usagers.

Des contrôles d’anomalies fonctionnelles sont effectués sur l’objet requête reçu par la calculette avec retour de codes d’erreurs spécifiques.

Ces contrôles effectués, l’application des barèmes et le lancement de l’ensemble des règles de gestion définies par la législation fiscale sont réalisés.

Les barèmes applicables sont définis dans deux fichiers XML, « ./resources/baremes.xml » et « ./resources/valeurs_baremes.xml ».  
Ils permettent d’effectuer les calculs à partir des taux d’imposition applicables :
- le fichier « baremes.xml » fournit une définition des barèmes en associant un code barème à un libellé,
- le fichier « valeur_baremes.xml » permet à partir du code barème de choisir le taux applicable en fonction de la date de début de validité de ce dernier.

Les règles de gestion ont toutes une classe dédiée, « CalculIsXXImpl.java » localisée sous « ./java/fr/gouv/impots/appli/capromas/domaine/is ».  

Le lancement d’une règle de gestion est conditionné par la présence et la valorisation de références spécifiques définies comme paramètres d’entrée accompagnées d’un barème précédemment calculé.

De nouvelles références définies en tant que paramètres de sortie participant à la réponse de l’offre de service sont alors valorisées.

### L’arborescence
![arborescence](arborescence.jpg)