/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.persistance;

import java.util.ArrayList;
import java.util.Iterator;

import fr.gouv.impots.appli.capromas.transverse.PValeurBaremeValeur;
import fr.gouv.impots.appli.capromas.transverse.PValeurBaremeValeurListe;
import fr.gouv.impots.appli.capromas.transverse.ValeurBaremeValeur;
import fr.gouv.impots.appli.capromas.transverse.ValeursBaremeSansTrancheValeur;
import fr.gouv.impots.appli.capromas.transverse.configuration.RestitutionProprietes;
import fr.gouv.impots.appli.commun.frameworks.util.serialisation.MappingObjetXML;
import fr.gouv.impots.appli.commun.frameworks.util.serialisation.MappingObjetXMLFactory;
import fr.gouv.impots.appli.commun.frameworks.util.serialisation.MappingObjetXMLFactoryInterface;

public final class RechercheValeurBaremeDaoEntite {
    
    private static String cheminDonneesValeursParDefaut = "conf.valeurs_baremes";
    
    private transient PValeurBaremeValeurListe listeValeursBaremes;
    
    private String cheminFichierMappingValeurs;
    
    private String cheminDataValeurs;
    
    private transient MappingObjetXML mappingObjetXML;
    
    public RechercheValeurBaremeDaoEntite() {
        super();
        
        this.cheminDataValeurs = RestitutionProprietes.getChargeurProprietes().getPropriete(cheminDonneesValeursParDefaut);
    }
    
    public static void setCheminDonneesValeurs(String newCheminDonneesValeursParDefaut) {
        cheminDonneesValeursParDefaut = newCheminDonneesValeursParDefaut;
    }
    
    public void setCheminFichierMappingValeurs(String newCheminFichierMappingValeurs) {
        this.cheminFichierMappingValeurs = newCheminFichierMappingValeurs;
    }
    
    public String getCheminFichierMappingValeurs() {
        return this.cheminFichierMappingValeurs;
    }
    
    public void setCheminDataValeurs(String newCheminDataValeurs) {
        this.cheminDataValeurs = newCheminDataValeurs;
    }
    
    public String getCheminDataValeurs() {
        return this.cheminDataValeurs;
    }
    
    public void init() {
        MappingObjetXMLFactoryInterface factory = new MappingObjetXMLFactory();
        mappingObjetXML = factory.obtenirMappingParClasse(PValeurBaremeValeurListe.class);
        
        this.listeValeursBaremes = this.chargerValeursBaremesDepuisXml();
    }
    
    private PValeurBaremeValeurListe chargerValeursBaremesDepuisXml() {
        PValeurBaremeValeurListe listeValeurs = (PValeurBaremeValeurListe) mappingObjetXML.deserialiserFichierExterne(this.cheminDataValeurs);
        
        return listeValeurs;
    }
    
    public ValeursBaremeSansTrancheValeur restituerValeursBaremeSansTranche(String pCdBareme) {
        ValeursBaremeSansTrancheValeur resultat = new ValeursBaremeSansTrancheValeur();
        ArrayList<PValeurBaremeValeur> valeursBaremeTrouvees = new ArrayList<PValeurBaremeValeur>();
        ValeurBaremeValeur[] tableauValeursBaremes = null;
        
        Iterator<PValeurBaremeValeur> iterateur = this.listeValeursBaremes.getValeurs().iterator();
        
        while (iterateur.hasNext()) {
            PValeurBaremeValeur valeurBaremeTrouve = iterateur.next();
            
            if ((valeurBaremeTrouve.getCdBareme() != null) && valeurBaremeTrouve.getCdBareme().equals(pCdBareme)) {
                valeursBaremeTrouvees.add(this.restituerValeurBareme(valeurBaremeTrouve.getUidValeurBareme()));
            }
        }
        
        if (valeursBaremeTrouvees.size() > 0) {
            tableauValeursBaremes = new ValeurBaremeValeur[valeursBaremeTrouvees.size()];
            tableauValeursBaremes = (ValeurBaremeValeur[]) valeursBaremeTrouvees.toArray(tableauValeursBaremes);
            resultat.setTheValeurBaremeValeur(tableauValeursBaremes);
            resultat.setNbValBareme(tableauValeursBaremes.length);
        }
        
        return resultat;
    }
    
    public PValeurBaremeValeur restituerValeurBareme(String pUidValeurBareme) {
        
        PValeurBaremeValeur resultat = null;
        
        Iterator<PValeurBaremeValeur> iterateur = this.listeValeursBaremes.getValeurs().iterator();
        
        while (iterateur.hasNext()) {
            PValeurBaremeValeur valeurBaremeTrouve = iterateur.next();
            
            if ((valeurBaremeTrouve.getUidValeurBareme() != null) && valeurBaremeTrouve.getUidValeurBareme().equals(pUidValeurBareme)) {
                resultat = valeurBaremeTrouve;
                break;
            }
        }
        
        return resultat;
    }
}
