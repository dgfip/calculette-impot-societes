/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.persistance;

import java.util.Iterator;

import fr.gouv.impots.appli.capromas.transverse.DonneesBareme;
import fr.gouv.impots.appli.capromas.transverse.baremes.BaremeGeneralValeur;
import fr.gouv.impots.appli.capromas.transverse.baremes.DescriptifBaremeValeur;
import fr.gouv.impots.appli.capromas.transverse.baremes.DescriptifBaremeValeurBrut;
import fr.gouv.impots.appli.capromas.transverse.baremes.DescriptifBaremeValeurListe;
import fr.gouv.impots.appli.capromas.transverse.configuration.RestitutionProprietes;
import fr.gouv.impots.appli.commun.frameworks.util.serialisation.MappingObjetXML;
import fr.gouv.impots.appli.commun.frameworks.util.serialisation.MappingObjetXMLFactory;
import fr.gouv.impots.appli.commun.frameworks.util.serialisation.MappingObjetXMLFactoryInterface;

public final class TraitementBaremeDaoEntite {
    
    private static String cheminDonneesBaremesParDefaut = "conf.baremes";
    
    private String cheminDataBaremes;
    
    private transient DescriptifBaremeValeurListe listeDescriptifBaremeValeurInstance;
    
    private transient MappingObjetXML mappingObjetXML;
    
    public TraitementBaremeDaoEntite() {
        super();
        this.cheminDataBaremes = RestitutionProprietes.getChargeurProprietes().getPropriete(cheminDonneesBaremesParDefaut);
    }
    
    public static void setCheminDonneesBaremesParDefaut(String newCheminDonneesBaremesParDefaut) {
        cheminDonneesBaremesParDefaut = newCheminDonneesBaremesParDefaut;
    }
    
    public String getCheminDataBaremes() {
        return cheminDataBaremes;
    }
    
    public void setCheminDataBaremes(String newCheminDataBaremes) {
        this.cheminDataBaremes = newCheminDataBaremes;
    }
    
    public void init() {
        MappingObjetXMLFactoryInterface factory = new MappingObjetXMLFactory();
        mappingObjetXML = factory.obtenirMappingParClasses(new Class[] { DescriptifBaremeValeurListe.class });
        this.listeDescriptifBaremeValeurInstance = (DescriptifBaremeValeurListe) mappingObjetXML.deserialiserFichierExterne(this.cheminDataBaremes);
    }
    
    public boolean consulterBareme(DescriptifBaremeValeur pDescriptif) {
        
        boolean resultat = false;
        
        String codeBaremeCible = pDescriptif.getCdBareme();
        
        Iterator<DescriptifBaremeValeurBrut> iterateur = this.listeDescriptifBaremeValeurInstance.getDescriptifsBaremes().iterator();
        
        while (iterateur.hasNext() && !resultat) {
            DescriptifBaremeValeurBrut baremeBrut = iterateur.next();
            
            if ((baremeBrut.getCdBareme() != null) && baremeBrut.getCdBareme().equals(codeBaremeCible)) {
                
                pDescriptif.setLbBareme(baremeBrut.getLbBareme());
                pDescriptif.setDtFinVldtBareme(baremeBrut.getDtFinVldtBareme());
                pDescriptif.setTypeUtilite(baremeBrut.getTypeUtilite());
                
                resultat = true;
            }
        }
        
        return resultat;
    }
    
    public void determinerGeneralite(DescriptifBaremeValeur pDescriptif) {
        
        String codeBaremeCible = pDescriptif.getCdBareme();
        
        Iterator<DescriptifBaremeValeurBrut> iterateur = this.listeDescriptifBaremeValeurInstance.getDescriptifsBaremes().iterator();
        
        while (iterateur.hasNext()) {
            DescriptifBaremeValeurBrut baremeBrut = iterateur.next();
            
            if ((baremeBrut.getCdBareme() != null) && baremeBrut.getCdBareme().equals(codeBaremeCible)) {
                
                String typeUtilite = baremeBrut.getTypeUtilite();
                
                pDescriptif.setOnGeneral(!DonneesBareme.TYPE_UTILITE_ATT.equals(typeUtilite));
                
                break;
            }
        }
    }
    
    public void determinerIndividualisation(DescriptifBaremeValeur pDescriptif) {
    }
    
    public void consulterBaremeAttributaire(DescriptifBaremeValeur pDescriptif) {
        
        String codeBaremeCible = pDescriptif.getCdBareme();
        
        Iterator<DescriptifBaremeValeurBrut> iterateur = this.listeDescriptifBaremeValeurInstance.getDescriptifsBaremes().iterator();
        
        while (iterateur.hasNext()) {
            DescriptifBaremeValeurBrut baremeBrut = iterateur.next();
            
            if ((baremeBrut.getCdBareme() != null) && baremeBrut.getCdBareme().equals(codeBaremeCible)) {
                
                break;
            }
        }
    }
    
    public void consulterBaremeLimite(String pCdBarGen, BaremeGeneralValeur pDescriptif) {
        
        Iterator<DescriptifBaremeValeurBrut> iterateur = this.listeDescriptifBaremeValeurInstance.getDescriptifsBaremes().iterator();
        
        while (iterateur.hasNext()) {
            DescriptifBaremeValeurBrut baremeBrut = iterateur.next();
            
            if ((baremeBrut.getCdBareme() != null) && baremeBrut.getCdBareme().equals(pCdBarGen)) {
                
                pDescriptif.getBaremeLimite().setSeuilPlafond(baremeBrut.getSeuilPlafond());
                
                boolean estAnnuel = "O".equals(baremeBrut.getOnAnnuel());
                
                pDescriptif.getBaremeLimite().setOnAnnuel(estAnnuel);
                
                break;
            }
        }
    }
    
    public void consulterBaremeTst(String pCdBarGen, BaremeGeneralValeur pDescriptif) {
        
        Iterator<DescriptifBaremeValeurBrut> iterateur = this.listeDescriptifBaremeValeurInstance.getDescriptifsBaremes().iterator();
        
        while (iterateur.hasNext()) {
            DescriptifBaremeValeurBrut baremeBrut = iterateur.next();
            
            if ((baremeBrut.getCdBareme() != null) && baremeBrut.getCdBareme().equals(pCdBarGen)) {
                
                boolean estUnitaire = "O".equals(baremeBrut.getOnUnitaire());
                
                pDescriptif.getBaremeTst().setOnUnitaire(estUnitaire);
                
                break;
            }
        }
    }
    
    public void consulterBaremeTat(String pCdBarGen, BaremeGeneralValeur pDescriptif) {
        
        Iterator<DescriptifBaremeValeurBrut> iterateur = this.listeDescriptifBaremeValeurInstance.getDescriptifsBaremes().iterator();
        
        while (iterateur.hasNext()) {
            DescriptifBaremeValeurBrut baremeBrut = iterateur.next();
            
            if ((baremeBrut.getCdBareme() != null) && baremeBrut.getCdBareme().equals(pCdBarGen)) {
                
                boolean estUnitaire = "O".equals(baremeBrut.getOnUnitaire());
                pDescriptif.getBaremeTat().setOnUnitaire(estUnitaire);
                
                boolean estCumulatif = "O".equals(baremeBrut.getOnCumulatif());
                pDescriptif.getBaremeTat().setOnCumulatif(estCumulatif);
                
                boolean estIncluseBorne = "O".equals(baremeBrut.getOnBorneInclu());
                pDescriptif.getBaremeTat().setOnBorneInclu(estIncluseBorne);
                
                boolean estMinimaleBorne = "O".equals(baremeBrut.getOnBorneMini());
                pDescriptif.getBaremeTat().setOnBorneMini(estMinimaleBorne);
                
                break;
            }
        }
    }
    
}
