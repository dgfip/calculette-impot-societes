/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.persistance;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import fr.gouv.impots.appli.capromas.transverse.configuration.RestitutionProprietes;
import fr.gouv.impots.appli.capromas.transverse.udd.anomalies.AnomalieValeur;
import fr.gouv.impots.appli.capromas.transverse.udd.anomalies.AnomalieValeurListe;
import fr.gouv.impots.appli.commun.frameworks.gestionerreurs.exceptions.TechDysfonctionnementErreur;
import fr.gouv.impots.appli.commun.frameworks.gestionerreurs.transverse.CodeErreurImpl;
import fr.gouv.impots.appli.commun.frameworks.gestionerreurs.transverse.GestionnaireErreurs;
import fr.gouv.impots.appli.commun.frameworks.util.serialisation.MappingObjetXML;
import fr.gouv.impots.appli.commun.frameworks.util.serialisation.MappingObjetXMLFactory;
import fr.gouv.impots.appli.commun.frameworks.util.serialisation.MappingObjetXMLFactoryInterface;

public final class ConstructionAnomalieDaoEntite {
    
    private static final String CHEMIN_DONNEES_ANOMALIES = "conf.anomalies";
    
    private String cheminDonneesAnomalies;
    
    private transient Map<String, AnomalieValeur> tableAnomalies;
    
    private transient MappingObjetXML mappingObjetXML;
    
    public ConstructionAnomalieDaoEntite() {
        super();
        this.cheminDonneesAnomalies = RestitutionProprietes.getChargeurProprietes().getPropriete(CHEMIN_DONNEES_ANOMALIES);
    }
    
    public void setCheminDonneesAnomalies(String newCheminDonneesAnomalies) {
        this.cheminDonneesAnomalies = newCheminDonneesAnomalies;
    }
    
    public String getCheminDonneesAnomalies() {
        return this.cheminDonneesAnomalies;
    }
    
    public void init() {
        MappingObjetXMLFactoryInterface factory = new MappingObjetXMLFactory();
        mappingObjetXML = factory.obtenirMappingParClasse(AnomalieValeurListe.class);
        
        AnomalieValeurListe listeAnomaliesValeur = chargerListeAnomaliesDepuisXML();
        
        tableAnomalies = new HashMap<String, AnomalieValeur>();
        
        Iterator<AnomalieValeur> iterateur = listeAnomaliesValeur.getAnomaliesValeur().iterator();
        
        while (iterateur.hasNext()) {
            AnomalieValeur anomalie = iterateur.next();
            
            if (anomalie != null) {
                tableAnomalies.put(anomalie.getCode(), anomalie);
            }
        }
    }
    
    public String construireAnomalie(String codeAnomalie) throws TechDysfonctionnementErreur {
        String retour = null;
        
        if ((codeAnomalie != null) && (this.tableAnomalies != null) && this.tableAnomalies.containsKey(codeAnomalie)) {
            retour = ((AnomalieValeur) tableAnomalies.get(codeAnomalie)).getLibelle();
        } else {
            TechDysfonctionnementErreur erreurTechnique = (TechDysfonctionnementErreur) GestionnaireErreurs.construireErreur(CodeErreurImpl.CODE_DYSF_RESSOURCES);
            throw erreurTechnique;
        }
        
        return retour;
    }
    
    private AnomalieValeurListe chargerListeAnomaliesDepuisXML() {
        AnomalieValeurListe listeValeurs = (AnomalieValeurListe) mappingObjetXML.deserialiserFichierExterne(this.cheminDonneesAnomalies);
        
        return listeValeurs;
    }
}
