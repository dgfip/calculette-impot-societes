/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.domaine;

import java.math.BigDecimal;

import fr.gouv.impots.appli.capromas.transverse.udd.metier.ElementFiscalValeur;

public final class UtilitaireMetier {
    
    public static Double arrondir(Double pValeur) {
        double resultat = pValeur.doubleValue();
        
        if (resultat >= 0) {
            resultat = Math.round(resultat);
        }

        return Double.valueOf(resultat);
    }
    
    public static Double additionner(ElementFiscalValeur... elements) {
        BigDecimal resultat = new BigDecimal("0");
        for (ElementFiscalValeur element : elements) {
            if (estPresentEtValorise(element))
                resultat = resultat.add(new BigDecimal(element.getVlEFisc().toString()));
        }
        return Double.valueOf(resultat.doubleValue());
    }
    
    public static Double soustraire(Double element1, Double element2) {
        BigDecimal elt1 = new BigDecimal(element1.toString());
        BigDecimal elt2 = new BigDecimal(element2.toString());
        
        return Double.valueOf(elt1.subtract(elt2).doubleValue());
    }
    
    public static Double soustraire(ElementFiscalValeur elementFiscalValeur1, ElementFiscalValeur elementFiscalValeur2) {
        return soustraire(elementFiscalValeur1.getVlEFisc(), elementFiscalValeur2.getVlEFisc());
    }
    
    public static Double multiplier(Double element1, Double element2) {
        BigDecimal elt1 = new BigDecimal(String.valueOf(element1));
        BigDecimal elt2 = new BigDecimal(String.valueOf(element2));
        
        return Double.valueOf(elt1.multiply(elt2).doubleValue());
    }

    public static Boolean estPresentEtValorise(ElementFiscalValeur ef) {
        return (ef != null && ef.estValorise());
    }
    
}