/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.domaine.is;

import fr.gouv.impots.appli.capromas.domaine.CalculElementFiscalMetier;
import fr.gouv.impots.appli.capromas.domaine.UtilitaireMetier;
import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieErreur;
import fr.gouv.impots.appli.capromas.transverse.udd.metier.ElementFiscalCalculeValeur;
import fr.gouv.impots.appli.capromas.transverse.udd.metier.ElementFiscalValeur;
import fr.gouv.impots.appli.capromas.domaine.CodesElementsFiscaux;
import fr.gouv.impots.appli.capromas.domaine.TableElementFiscal;
import fr.gouv.impots.appli.commun.frameworks.gestionerreurs.exceptions.TechDysfonctionnementErreur;

public final class CalculIs04Impl {
    
    private static String[] codesEnEntree = { CodesElementsFiscaux.EF_500021, CodesElementsFiscaux.EF_500022, CodesElementsFiscaux.EF_500023, CodesElementsFiscaux.EF_500024 };
    
    private static String[] codesEnSortie = { CodesElementsFiscaux.EF_500023, CodesElementsFiscaux.EF_500024 };
    
    public CalculIs04Impl() {
        super();
    }
    
    public ElementFiscalCalculeValeur[] calculerMontantTotal(ElementFiscalValeur[] elementsFiscauxACalculer) {
        
        ElementFiscalValeur[] tableEFValorises = getCalculElementFiscalMetier().valoriserEltZero(codesEnEntree, elementsFiscauxACalculer);
        
        TableElementFiscal tableElementFiscalValeur = new TableElementFiscal(tableEFValorises);
        
        Double valeurCalculee[] = new Double[2];
        
        ElementFiscalValeur ef500021 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_500021, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef500022 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_500022, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        if (ef500021.getVlEFisc() >= ef500022.getVlEFisc()) {
            valeurCalculee[0] = UtilitaireMetier.soustraire(ef500021.getVlEFisc(), ef500022.getVlEFisc());
            valeurCalculee[1] = 0.0;
        } else {
            valeurCalculee[0] = 0.0;
            valeurCalculee[1] = UtilitaireMetier.soustraire(ef500022.getVlEFisc(), ef500021.getVlEFisc());
        }
        
        ElementFiscalCalculeValeur eltCalcule1 = new ElementFiscalCalculeValeur(CodesElementsFiscaux.EF_500023, ElementFiscalCalculeValeur.NUMERO_DISTINCTION_NON_DEFINI, valeurCalculee[0]);
        ElementFiscalCalculeValeur eltCalcule2 = new ElementFiscalCalculeValeur(CodesElementsFiscaux.EF_500024, ElementFiscalCalculeValeur.NUMERO_DISTINCTION_NON_DEFINI, valeurCalculee[1]);
        
        ElementFiscalCalculeValeur[] tabEfResultat = new ElementFiscalCalculeValeur[] { eltCalcule1, eltCalcule2 };
        
        return tabEfResultat;
    }
    
    public String[] getCodesEFiscUtiles() {
        return codesEnEntree;
    }
    
    public String[] getCodesEFiscResultat() {
        return codesEnSortie;
    }
    
    public ElementFiscalCalculeValeur[] calculerResultat(ElementFiscalValeur[] listeEF, Integer noDistinctionEf) throws AnomalieErreur, TechDysfonctionnementErreur {
        return null;
    }
    
    public CalculElementFiscalMetier getCalculElementFiscalMetier() {
        return new CalculElementFiscalMetier();
    }
}
