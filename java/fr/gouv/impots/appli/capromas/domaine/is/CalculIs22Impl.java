/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.domaine.is;

import fr.gouv.impots.appli.capromas.domaine.CalculElementFiscalMetier;
import fr.gouv.impots.appli.capromas.domaine.UtilitaireMetier;
import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieErreur;
import fr.gouv.impots.appli.capromas.transverse.udd.metier.ElementFiscalCalculeValeur;
import fr.gouv.impots.appli.capromas.transverse.udd.metier.ElementFiscalValeur;
import fr.gouv.impots.appli.capromas.domaine.CodesElementsFiscaux;
import fr.gouv.impots.appli.capromas.domaine.TableElementFiscal;
import fr.gouv.impots.appli.commun.frameworks.gestionerreurs.exceptions.TechDysfonctionnementErreur;

public final class CalculIs22Impl {
    
    private static String[] codesEnEntree = { CodesElementsFiscaux.EF_906464, CodesElementsFiscaux.EF_906465, CodesElementsFiscaux.EF_906570, CodesElementsFiscaux.EF_906571 };
    
    private static String[] codesEnSortie = { CodesElementsFiscaux.EF_906570, CodesElementsFiscaux.EF_906571 };
    
    public CalculIs22Impl() {
        super();
    }
    
    public ElementFiscalCalculeValeur[] calculerTotal(ElementFiscalValeur[] elementsFiscauxACalculer) {
        
        ElementFiscalValeur[] tableEFValorises = getCalculElementFiscalMetier().valoriserEltZero(codesEnEntree, elementsFiscauxACalculer);
        
        TableElementFiscal tableElementFiscalValeur = new TableElementFiscal(tableEFValorises);
        
        ElementFiscalValeur ef906464 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_906464, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef906465 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_906465, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        
        Double[] valeurCalculee = { 0.0, 0.0 };
        
        if (ef906464.getVlEFisc() >= ef906465.getVlEFisc()) {
            valeurCalculee[0] = UtilitaireMetier.soustraire(ef906464.getVlEFisc(), ef906465.getVlEFisc());
            valeurCalculee[1] = 0.0;
        } else {
            valeurCalculee[0] = 0.0;
            valeurCalculee[1] = UtilitaireMetier.soustraire(ef906465.getVlEFisc(), ef906464.getVlEFisc());
        }
        
        ElementFiscalCalculeValeur eltCalcule1 = new ElementFiscalCalculeValeur(CodesElementsFiscaux.EF_906570, ElementFiscalCalculeValeur.NUMERO_DISTINCTION_NON_DEFINI, valeurCalculee[0]);
        ElementFiscalCalculeValeur eltCalcule2 = new ElementFiscalCalculeValeur(CodesElementsFiscaux.EF_906571, ElementFiscalCalculeValeur.NUMERO_DISTINCTION_NON_DEFINI, valeurCalculee[1]);
        
        ElementFiscalCalculeValeur[] tabEfResultat = new ElementFiscalCalculeValeur[] { eltCalcule1, eltCalcule2 };
        
        return tabEfResultat;
    }
    
    public String[] getCodesEFiscUtiles() {
        return codesEnEntree;
    }
    
    public String[] getCodesEFiscResultat() {
        return codesEnSortie;
    }
    
    public ElementFiscalCalculeValeur[] calculerResultat(ElementFiscalValeur[] listeEF, Integer noDistinctionEf) throws AnomalieErreur, TechDysfonctionnementErreur {
        return null;
    }
    
    public CalculElementFiscalMetier getCalculElementFiscalMetier() {
        return new CalculElementFiscalMetier();
    }
}