/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.domaine.is;

import fr.gouv.impots.appli.capromas.domaine.CalculElementFiscalMetier;
import fr.gouv.impots.appli.capromas.domaine.UtilitaireMetier;
import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieErreur;
import fr.gouv.impots.appli.capromas.transverse.udd.metier.ElementFiscalCalculeValeur;
import fr.gouv.impots.appli.capromas.transverse.udd.metier.ElementFiscalValeur;
import fr.gouv.impots.appli.capromas.domaine.CodesElementsFiscaux;
import fr.gouv.impots.appli.capromas.domaine.TableElementFiscal;
import fr.gouv.impots.appli.commun.frameworks.gestionerreurs.exceptions.TechDysfonctionnementErreur;

public final class CalculIs05Impl {
    
    private static String[] codesEnEntree = { CodesElementsFiscaux.EF_500024, CodesElementsFiscaux.EF_500025, CodesElementsFiscaux.EF_906470, CodesElementsFiscaux.EF_500026 };
    
    private static String[] codesEnSortie = { CodesElementsFiscaux.EF_500026 };
    
    public CalculIs05Impl() {
        super();
    }
    
    public ElementFiscalCalculeValeur[] calculerMontantTotalExcedent(ElementFiscalValeur[] elementsFiscauxACalculer) {
        
        ElementFiscalValeur[] tableEFValorises = getCalculElementFiscalMetier().valoriserEltZero(codesEnEntree, elementsFiscauxACalculer);
        
        TableElementFiscal tableElementFiscalValeur = new TableElementFiscal(tableEFValorises);
        
        ElementFiscalValeur ef500024 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_500024, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef500025 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_500025, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef906470 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_906470, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        
        ElementFiscalValeur[] table = { ef500025, ef906470 };
        Double calcul = UtilitaireMetier.additionner(table);
        Double valeurCalculee = UtilitaireMetier.soustraire(ef500024.getVlEFisc(), calcul);
        
        if (valeurCalculee < 0.0) {
            valeurCalculee = 0.0;
        }
        
        ElementFiscalCalculeValeur eltCalcule = new ElementFiscalCalculeValeur(CodesElementsFiscaux.EF_500026, ElementFiscalCalculeValeur.NUMERO_DISTINCTION_NON_DEFINI, valeurCalculee);
        
        ElementFiscalCalculeValeur[] tabEfResultat = new ElementFiscalCalculeValeur[] { eltCalcule };
        
        return tabEfResultat;
    }
    
    public String[] getCodesEFiscUtiles() {
        return codesEnEntree;
    }
    
    public String[] getCodesEFiscResultat() {
        return codesEnSortie;
    }
    
    public ElementFiscalCalculeValeur[] calculerResultat(ElementFiscalValeur[] listeEF, Integer noDistinctionEf) throws AnomalieErreur, TechDysfonctionnementErreur {
        return null;
    }
    
    public CalculElementFiscalMetier getCalculElementFiscalMetier() {
        return new CalculElementFiscalMetier();
    }
}
