/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.domaine.is;

import fr.gouv.impots.appli.capromas.domaine.CalculElementFiscalMetier;
import fr.gouv.impots.appli.capromas.domaine.UtilitaireMetier;
import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieErreur;
import fr.gouv.impots.appli.capromas.transverse.udd.metier.ElementFiscalCalculeValeur;
import fr.gouv.impots.appli.capromas.transverse.udd.metier.ElementFiscalValeur;
import fr.gouv.impots.appli.capromas.domaine.CodesElementsFiscaux;
import fr.gouv.impots.appli.capromas.domaine.TableElementFiscal;
import fr.gouv.impots.appli.commun.frameworks.gestionerreurs.exceptions.TechDysfonctionnementErreur;

public final class CalculIs14Impl {
    
    private static String[] codesEnEntree = { CodesElementsFiscaux.EF_500038, CodesElementsFiscaux.EF_906451, CodesElementsFiscaux.EF_904112, CodesElementsFiscaux.EF_904111, CodesElementsFiscaux.EF_906455, CodesElementsFiscaux.EF_500036,
            CodesElementsFiscaux.EF_906452, CodesElementsFiscaux.EF_906453, CodesElementsFiscaux.EF_500034, CodesElementsFiscaux.EF_906454, CodesElementsFiscaux.EF_500054, CodesElementsFiscaux.EF_906456, CodesElementsFiscaux.EF_907017,
            CodesElementsFiscaux.EF_907018 };
    
    private static String[] codesEnSortie = { CodesElementsFiscaux.EF_906456 };
    
    public CalculIs14Impl() {
        super();
    }
    
    public ElementFiscalCalculeValeur[] calculerTotal(ElementFiscalValeur[] elementsFiscauxACalculer) {
        
        ElementFiscalValeur[] tableEFValorises = getCalculElementFiscalMetier().valoriserEltZero(codesEnEntree, elementsFiscauxACalculer);
        
        TableElementFiscal tableElementFiscalValeur = new TableElementFiscal(tableEFValorises);
        
        ElementFiscalValeur ef500038 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_500038, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef906451 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_906451, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef904112 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_904112, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef904111 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_904111, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef906455 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_906455, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef500036 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_500036, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef906452 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_906452, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef906453 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_906453, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef500034 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_500034, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef906454 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_906454, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef500054 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_500054, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef907017 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_907017, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef907018 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_907018, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        
        Double valeurCalculee = null;
        ElementFiscalValeur[] table = { ef500038, ef906451, ef904112, ef906455, ef500036, ef906452, ef906453, ef500034, ef906454, ef500054, ef907017 };
        
        valeurCalculee = UtilitaireMetier.soustraire(UtilitaireMetier.additionner(table), Math.min(ef904111.getVlEFisc(), ef904112.getVlEFisc()));
        valeurCalculee = UtilitaireMetier.soustraire(valeurCalculee, Math.min(ef907017.getVlEFisc(), ef907018.getVlEFisc()));
        
        ElementFiscalCalculeValeur eltCalcule = new ElementFiscalCalculeValeur(CodesElementsFiscaux.EF_906456, ElementFiscalCalculeValeur.NUMERO_DISTINCTION_NON_DEFINI, valeurCalculee);
        
        ElementFiscalCalculeValeur[] tabEfResultat = new ElementFiscalCalculeValeur[] { eltCalcule };
        
        return tabEfResultat;
    }
    
    public String[] getCodesEFiscUtiles() {
        return codesEnEntree;
    }
    
    public String[] getCodesEFiscResultat() {
        return codesEnSortie;
    }
    
    public ElementFiscalCalculeValeur[] calculerResultat(ElementFiscalValeur[] listeEF, Integer noDistinctionEf) throws AnomalieErreur, TechDysfonctionnementErreur {
        return null;
    }
    
    public CalculElementFiscalMetier getCalculElementFiscalMetier() {
        return new CalculElementFiscalMetier();
    }
}