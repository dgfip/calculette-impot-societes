/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.domaine.is;

import fr.gouv.impots.appli.capromas.domaine.CalculElementFiscalMetier;
import fr.gouv.impots.appli.capromas.domaine.UtilitaireMetier;
import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieErreur;
import fr.gouv.impots.appli.capromas.transverse.udd.metier.ElementFiscalCalculeValeur;
import fr.gouv.impots.appli.capromas.transverse.udd.metier.ElementFiscalValeur;
import fr.gouv.impots.appli.capromas.domaine.CodesElementsFiscaux;
import fr.gouv.impots.appli.capromas.domaine.TableElementFiscal;
import fr.gouv.impots.appli.commun.frameworks.gestionerreurs.exceptions.TechDysfonctionnementErreur;

public final class CalculIs16Impl {
    
    private static String[] codesEnEntree = { CodesElementsFiscaux.EF_500031, CodesElementsFiscaux.EF_500032, CodesElementsFiscaux.EF_500033, CodesElementsFiscaux.EF_500042, CodesElementsFiscaux.EF_500043, CodesElementsFiscaux.EF_500047,
            CodesElementsFiscaux.EF_500049, CodesElementsFiscaux.EF_500052, CodesElementsFiscaux.EF_900763, CodesElementsFiscaux.EF_900764, CodesElementsFiscaux.EF_900767, CodesElementsFiscaux.EF_900769, CodesElementsFiscaux.EF_900771,
            CodesElementsFiscaux.EF_900772, CodesElementsFiscaux.EF_902447, CodesElementsFiscaux.EF_500053, CodesElementsFiscaux.EF_906459, CodesElementsFiscaux.EF_904110, CodesElementsFiscaux.EF_906507, CodesElementsFiscaux.EF_906508,
            CodesElementsFiscaux.EF_906506, CodesElementsFiscaux.EF_908550 };
    
    private static String[] codesEnSortie = { CodesElementsFiscaux.EF_906459 };
    
    public CalculIs16Impl() {
        super();
    }
    
    public ElementFiscalCalculeValeur[] calculerTotal(ElementFiscalValeur[] elementsFiscauxACalculer) {
        
        ElementFiscalValeur[] tableEFValorises = getCalculElementFiscalMetier().valoriserEltZero(codesEnEntree, elementsFiscauxACalculer);
        
        TableElementFiscal tableElementFiscalValeur = new TableElementFiscal(tableEFValorises);
        
        ElementFiscalValeur ef500031 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_500031, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef500032 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_500032, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef500033 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_500033, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef500042 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_500042, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef500043 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_500043, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef500047 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_500047, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef500049 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_500049, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef500052 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_500052, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef900763 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_900763, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef900764 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_900764, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef900767 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_900767, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef900769 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_900769, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef900771 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_900771, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef900772 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_900772, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef902447 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_902447, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef500053 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_500053, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef904110 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_904110, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef906507 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_906507, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef906508 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_906508, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef906506 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_906506, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef908550 = tableElementFiscalValeur.getElementFiscal(CodesElementsFiscaux.EF_908550, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        
        Double valeurCalculee = null;
        ElementFiscalValeur[] table = { ef500031, ef500032, ef500033, ef500042, ef500043, ef500047, ef500049, ef500052, ef900763, ef900764, ef900767, ef900769, ef900771, ef900772, ef902447, ef500053, ef904110, ef906508, ef908550 };
        
        valeurCalculee = UtilitaireMetier.soustraire(UtilitaireMetier.additionner(table), Math.min(ef904110.getVlEFisc(), ef906507.getVlEFisc()));
        valeurCalculee = UtilitaireMetier.soustraire(valeurCalculee, Math.min(ef906508.getVlEFisc(), ef906506.getVlEFisc()));
        
        ElementFiscalCalculeValeur eltCalcule = new ElementFiscalCalculeValeur(CodesElementsFiscaux.EF_906459, ElementFiscalCalculeValeur.NUMERO_DISTINCTION_NON_DEFINI, valeurCalculee);
        
        ElementFiscalCalculeValeur[] tabEfResultat = new ElementFiscalCalculeValeur[] { eltCalcule };
        
        return tabEfResultat;
    }
    
    public String[] getCodesEFiscUtiles() {
        return codesEnEntree;
    }
    
    public String[] getCodesEFiscResultat() {
        return codesEnSortie;
    }
    
    public ElementFiscalCalculeValeur[] calculerResultat(ElementFiscalValeur[] listeEF, Integer noDistinctionEf) throws AnomalieErreur, TechDysfonctionnementErreur {
        return null;
    }
    
    public CalculElementFiscalMetier getCalculElementFiscalMetier() {
        return new CalculElementFiscalMetier();
    }
}