/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.domaine;

import java.util.Arrays;
import java.util.List;

import fr.gouv.impots.appli.capromas.transverse.udd.metier.ElementFiscalCalculeValeur;
import fr.gouv.impots.appli.capromas.transverse.udd.metier.ElementFiscalValeur;

public final class TableElementFiscalCalcule extends TableElementFiscal {
    
    private static final long serialVersionUID = -3501544025759868444L;
    
    public TableElementFiscalCalcule(final ElementFiscalCalculeValeur elementFiscalCalcule) {
        super();
        ajouterElementFiscal(elementFiscalCalcule);
    }
    
    public TableElementFiscalCalcule(final List<ElementFiscalCalculeValeur> listeEfC) {
        super();
        ajouterElementsFiscaux((ElementFiscalCalculeValeur[]) listeEfC.toArray(new ElementFiscalCalculeValeur[listeEfC.size()]));
    }
    
    public TableElementFiscalCalcule(final ElementFiscalCalculeValeur[] elementsFiscauxCalcules) {
        super();
        ajouterElementsFiscaux(elementsFiscauxCalcules);
    }
    
    public TableElementFiscalCalcule() {
        super();
    }
    
    public void ajouterElementFiscalCalcule(ElementFiscalCalculeValeur elementFiscal) {
        super.ajouterElementFiscal(elementFiscal);
    }
    
    public void ajouterElementsFiscauxCalcule(ElementFiscalCalculeValeur[] elementsFiscaux) {
        super.ajouterElementsFiscaux(elementsFiscaux);
    }
    
    public ElementFiscalCalculeValeur retirerElementFiscalCalcule(String codeElementFiscal, Integer numeroDistinction) {
        ElementFiscalCalculeValeur element = (ElementFiscalCalculeValeur) super.retirerElementFiscal(codeElementFiscal, numeroDistinction);
        
        return element;
    }
    
    public ElementFiscalCalculeValeur[] retirerElementsFiscauxCalcule(String codeElementFiscal) {
        ElementFiscalValeur[] retour = super.retirerElementsFiscaux(codeElementFiscal);
        ElementFiscalCalculeValeur[] tableauRetour = new ElementFiscalCalculeValeur[retour.length];
        tableauRetour = Arrays.asList(retour).toArray(tableauRetour);
        return tableauRetour;
    }
    
    public ElementFiscalCalculeValeur getElementFiscalCalculeSimple(String codeElementFiscal) {
        ElementFiscalValeur retour = super.getElementFiscal(codeElementFiscal, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        return (ElementFiscalCalculeValeur) retour;
    }
    
    public ElementFiscalCalculeValeur[] getElementsFiscauxCalcules(String codeElementFiscal) {
        ElementFiscalCalculeValeur[] tableauRetour = null;
        
        ElementFiscalValeur[] retour = super.getElementsFiscaux(codeElementFiscal);
        if (retour != null) {
            tableauRetour = new ElementFiscalCalculeValeur[retour.length];
            tableauRetour = Arrays.asList(retour).toArray(tableauRetour);
        }
        return tableauRetour;
    }
    
    public ElementFiscalCalculeValeur getElementFiscalCalcule(String codeElementFiscal, Integer numeroDistinction) {
        return (ElementFiscalCalculeValeur) super.getElementFiscal(codeElementFiscal, numeroDistinction);
    }
    
    public ElementFiscalCalculeValeur retirerElementFiscalCalculeSimple(String codeFiscal) {
        ElementFiscalCalculeValeur eltFiscalRetour = (ElementFiscalCalculeValeur) super.retirerElementFiscalSimple(codeFiscal);
        return eltFiscalRetour;
    }
    
    public ElementFiscalCalculeValeur[] getElementsFiscauxCalculeValeurs() {
        ElementFiscalValeur[] retour = super.getElementsFiscauxValeurs();
        ElementFiscalCalculeValeur[] tableauRetour = new ElementFiscalCalculeValeur[retour.length];
        
        tableauRetour = Arrays.asList(retour).toArray(tableauRetour);
        return tableauRetour;
    }
}
