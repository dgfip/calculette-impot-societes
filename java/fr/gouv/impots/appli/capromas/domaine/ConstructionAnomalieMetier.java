/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.domaine;

import java.util.Calendar;

import fr.gouv.impots.appli.capromas.persistance.ConstructionAnomalieDaoEntite;
import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieErreur;
import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieF007Erreur;
import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieF010Erreur;
import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieF020Erreur;
import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieF030Erreur;
import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieF031Erreur;
import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieF040Erreur;
import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieF045Erreur;
import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieF050Erreur;
import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieF051Erreur;
import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieF090Erreur;
import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieF100Erreur;
import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieF110Erreur;
import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieF120Erreur;
import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieF180Erreur;
import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieF190Erreur;
import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieF195Erreur;
import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieF200Erreur;
import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieF205Erreur;
import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieF210Erreur;
import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieF220Erreur;
import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieF225Erreur;
import fr.gouv.impots.appli.capromas.transverse.anomalies.ListeAnomaliesErreur;
import fr.gouv.impots.appli.capromas.transverse.baremes.DescriptifBaremeValeur;
import fr.gouv.impots.appli.capromas.transverse.udd.anomalies.AnomalieF007Valeur;
import fr.gouv.impots.appli.capromas.transverse.udd.anomalies.AnomalieF010Valeur;
import fr.gouv.impots.appli.capromas.transverse.udd.anomalies.AnomalieF020Valeur;
import fr.gouv.impots.appli.capromas.transverse.udd.anomalies.AnomalieF030Valeur;
import fr.gouv.impots.appli.capromas.transverse.udd.anomalies.AnomalieF031Valeur;
import fr.gouv.impots.appli.capromas.transverse.udd.anomalies.AnomalieF040Valeur;
import fr.gouv.impots.appli.capromas.transverse.udd.anomalies.AnomalieF045Valeur;
import fr.gouv.impots.appli.capromas.transverse.udd.anomalies.AnomalieF050Valeur;
import fr.gouv.impots.appli.capromas.transverse.udd.anomalies.AnomalieF051Valeur;
import fr.gouv.impots.appli.capromas.transverse.udd.anomalies.AnomalieF090Valeur;
import fr.gouv.impots.appli.capromas.transverse.udd.anomalies.AnomalieF100Valeur;
import fr.gouv.impots.appli.capromas.transverse.udd.anomalies.AnomalieF110Valeur;
import fr.gouv.impots.appli.capromas.transverse.udd.anomalies.AnomalieF120Valeur;
import fr.gouv.impots.appli.capromas.transverse.udd.anomalies.AnomalieF180Valeur;
import fr.gouv.impots.appli.capromas.transverse.udd.anomalies.AnomalieF190Valeur;
import fr.gouv.impots.appli.capromas.transverse.udd.anomalies.AnomalieF195Valeur;
import fr.gouv.impots.appli.capromas.transverse.udd.anomalies.AnomalieF200Valeur;
import fr.gouv.impots.appli.capromas.transverse.udd.anomalies.AnomalieF205Valeur;
import fr.gouv.impots.appli.capromas.transverse.udd.anomalies.AnomalieF210Valeur;
import fr.gouv.impots.appli.capromas.transverse.udd.anomalies.AnomalieF220Valeur;
import fr.gouv.impots.appli.capromas.transverse.udd.anomalies.AnomalieF225Valeur;
import fr.gouv.impots.appli.capromas.transverse.udd.anomalies.AnomalieValeur;
import fr.gouv.impots.appli.capromas.transverse.udd.metier.PeriodeFiscaleValeur;
import fr.gouv.impots.appli.commun.frameworks.gestionerreurs.exceptions.TechDysfonctionnementErreur;

public final class ConstructionAnomalieMetier {
    
    private ConstructionAnomalieDaoEntite constructionAnomalieDaoEntite;
    
    public ConstructionAnomalieMetier() {
        super();
    }
    
    public void setConstructionAnomalieDaoEntite(ConstructionAnomalieDaoEntite newDao) {
        this.constructionAnomalieDaoEntite = newDao;
    }
    
    public AnomalieErreur construireAnomalie(String codeAnomalie) {
        AnomalieValeur anomalieValeur = new AnomalieValeur(codeAnomalie);
        
        return new AnomalieErreur(anomalieValeur);
    }
    
    public AnomalieF007Erreur construireAnomalieF007(Calendar date) {
        AnomalieF007Valeur anomalieF007Valeur = new AnomalieF007Valeur();
        anomalieF007Valeur.setDate(date);
        
        return new AnomalieF007Erreur(anomalieF007Valeur);
    }
    
    public AnomalieF010Erreur construireAnomalieF010(PeriodeFiscaleValeur periodeFiscaleValeur, String refElfBase, String refElfResultat, DescriptifBaremeValeur descriptifBareme, Integer pNbValBareme) throws TechDysfonctionnementErreur {
        AnomalieF010Valeur anomalieF010Valeur = new AnomalieF010Valeur();
        anomalieF010Valeur.setCdBareme(descriptifBareme.getCdBareme());
        anomalieF010Valeur.setLbBareme(descriptifBareme.getLbBareme());
        anomalieF010Valeur.setDtFisc(periodeFiscaleValeur.getDtFisc());
        anomalieF010Valeur.setDtDebPerFisc(periodeFiscaleValeur.getDtDebPerFisc());
        anomalieF010Valeur.setDtFinPerFisc(periodeFiscaleValeur.getDtFinPerFisc());
        anomalieF010Valeur.setRefElFBase(refElfBase);
        anomalieF010Valeur.setRefElFResultat(refElfResultat);
        anomalieF010Valeur.setNbValBareme(pNbValBareme);
        
        return new AnomalieF010Erreur(anomalieF010Valeur);
    }
    
    public AnomalieF020Erreur construireAnomalieF020(String refElF) throws TechDysfonctionnementErreur {
        AnomalieF020Valeur anomalieF020Valeur = new AnomalieF020Valeur();
        anomalieF020Valeur.setRefElF(refElF);
        
        return new AnomalieF020Erreur(anomalieF020Valeur);
    }
    
    public AnomalieF030Erreur construireAnomalieF030(String refElF, Integer noDistinction) throws TechDysfonctionnementErreur {
        AnomalieF030Valeur anomalieF030Valeur = new AnomalieF030Valeur();
        anomalieF030Valeur.setRefElF(refElF);
        anomalieF030Valeur.setNoDistinction(noDistinction);
        
        return new AnomalieF030Erreur(anomalieF030Valeur);
    }
    
    public AnomalieF031Erreur construireAnomalieF031(String refElF) throws TechDysfonctionnementErreur {
        AnomalieF031Valeur anomalieF031Valeur = new AnomalieF031Valeur();
        anomalieF031Valeur.setRefElF(refElF);
        
        return new AnomalieF031Erreur(anomalieF031Valeur);
    }
    
    public AnomalieF040Erreur construireAnomalieF040(PeriodeFiscaleValeur periodeFiscaleValeur, String refEfBase, String refEfResultat, String[] listeCdBareme, String[] listeLbBareme) throws TechDysfonctionnementErreur {
        AnomalieF040Valeur anomalieF040Valeur = new AnomalieF040Valeur();
        anomalieF040Valeur.setDtFisc(periodeFiscaleValeur.getDtFisc());
        anomalieF040Valeur.setDtDebPerFisc(periodeFiscaleValeur.getDtDebPerFisc());
        anomalieF040Valeur.setDtFinPerFisc(periodeFiscaleValeur.getDtFinPerFisc());
        anomalieF040Valeur.setRefElFBase(refEfBase);
        anomalieF040Valeur.setRefElFResultat(refEfResultat);
        anomalieF040Valeur.setListeCdBareme(listeCdBareme);
        anomalieF040Valeur.setListeLbBaremes(listeLbBareme);
        
        return new AnomalieF040Erreur(anomalieF040Valeur);
    }
    
    public AnomalieF045Erreur construireAnomalieF045(PeriodeFiscaleValeur periodeFiscaleValeur, String refEfBase, String refEfResultat) throws TechDysfonctionnementErreur {
        AnomalieF045Valeur anomalieF045Valeur = new AnomalieF045Valeur();
        anomalieF045Valeur.setDtFisc(periodeFiscaleValeur.getDtFisc());
        anomalieF045Valeur.setDtDebPerFisc(periodeFiscaleValeur.getDtDebPerFisc());
        anomalieF045Valeur.setDtFinPerFisc(periodeFiscaleValeur.getDtFinPerFisc());
        anomalieF045Valeur.setRefElFBase(refEfBase);
        anomalieF045Valeur.setRefElFResultat(refEfResultat);
        
        return new AnomalieF045Erreur(anomalieF045Valeur);
    }
    
    public AnomalieF050Erreur construireAnomalieF050(PeriodeFiscaleValeur periodeFiscaleValeur, String refElfBase, String refElfResultat, DescriptifBaremeValeur descriptifBaremeValeur) throws TechDysfonctionnementErreur {
        AnomalieF050Valeur anomalieF050Valeur = new AnomalieF050Valeur();
        anomalieF050Valeur.setDtFisc(periodeFiscaleValeur.getDtFisc());
        anomalieF050Valeur.setDtDebPerFisc(periodeFiscaleValeur.getDtDebPerFisc());
        anomalieF050Valeur.setDtFinPerFisc(periodeFiscaleValeur.getDtFinPerFisc());
        anomalieF050Valeur.setRefElFBase(refElfBase);
        anomalieF050Valeur.setRefElFResultat(refElfResultat);
        anomalieF050Valeur.setCdBareme(descriptifBaremeValeur.getCdBareme());
        anomalieF050Valeur.setLbBareme(descriptifBaremeValeur.getLbBareme());
        
        return new AnomalieF050Erreur(anomalieF050Valeur);
    }
    
    public AnomalieF051Erreur construireAnomalieF051(String cdBareme) {
        AnomalieF051Valeur anomalieF051Valeur = new AnomalieF051Valeur();
        anomalieF051Valeur.setCdBareme(cdBareme);
        
        return new AnomalieF051Erreur(anomalieF051Valeur);
    }
    
    public AnomalieF090Erreur construireAnomalieF090() throws TechDysfonctionnementErreur {
        AnomalieF090Valeur anomalieF090Valeur = new AnomalieF090Valeur();
        
        return new AnomalieF090Erreur(anomalieF090Valeur);
    }
    
    public AnomalieF100Erreur construireAnomalieF100(String refElF) throws TechDysfonctionnementErreur {
        AnomalieF100Valeur anomalieF100Valeur = new AnomalieF100Valeur();
        anomalieF100Valeur.setRefElF(refElF);
        
        return new AnomalieF100Erreur(anomalieF100Valeur);
    }
    
    public AnomalieF110Erreur construireAnomalieF110(PeriodeFiscaleValeur periodeFiscaleValeur, String refElf) throws TechDysfonctionnementErreur {
        AnomalieF110Valeur anomalieF110Valeur = new AnomalieF110Valeur();
        
        anomalieF110Valeur.setDtFisc(periodeFiscaleValeur.getDtFisc());
        anomalieF110Valeur.setDtDebPerFisc(periodeFiscaleValeur.getDtDebPerFisc());
        anomalieF110Valeur.setDtFinPerFisc(periodeFiscaleValeur.getDtFinPerFisc());
        anomalieF110Valeur.setRefElF(refElf);
        
        return new AnomalieF110Erreur(anomalieF110Valeur);
    }
    
    public AnomalieF120Erreur construireAnomalieF120(PeriodeFiscaleValeur periodeFiscaleValeur, String refElf) throws TechDysfonctionnementErreur {
        AnomalieF120Valeur anomalieF120Valeur = new AnomalieF120Valeur();
        anomalieF120Valeur.setDtFisc(periodeFiscaleValeur.getDtFisc());
        anomalieF120Valeur.setDtDebPerFisc(periodeFiscaleValeur.getDtDebPerFisc());
        anomalieF120Valeur.setDtFinPerFisc(periodeFiscaleValeur.getDtFinPerFisc());
        anomalieF120Valeur.setRefElF(refElf);
        
        return new AnomalieF120Erreur(anomalieF120Valeur);
    }
    
    public AnomalieF180Erreur construireAnomalieF180(PeriodeFiscaleValeur periodeFiscaleValeur, String refElfBase, String refElfResultat, DescriptifBaremeValeur descriptifBaremeValeur, String pTypeUtilite) throws TechDysfonctionnementErreur {
        AnomalieF180Valeur anomalie = new AnomalieF180Valeur();
        anomalie.setDtFisc(periodeFiscaleValeur.getDtFisc());
        anomalie.setDtDebPerFisc(periodeFiscaleValeur.getDtDebPerFisc());
        anomalie.setDtFinPerFisc(periodeFiscaleValeur.getDtFinPerFisc());
        anomalie.setRefElFResultat(refElfResultat);
        anomalie.setRefElFBase(refElfBase);
        anomalie.setCdBareme(descriptifBaremeValeur.getCdBareme());
        anomalie.setLbBareme(descriptifBaremeValeur.getLbBareme());
        anomalie.setTypeUtilite(pTypeUtilite);
        
        return new AnomalieF180Erreur(anomalie);
    }
    
    public AnomalieF190Erreur construireAnomalieF190(PeriodeFiscaleValeur periodeFiscaleValeur, String refElfBase, String refElfResultat, DescriptifBaremeValeur descriptifBaremeValeur) throws TechDysfonctionnementErreur {
        AnomalieF190Valeur anomalieF190Valeur = new AnomalieF190Valeur();
        anomalieF190Valeur.setDtFisc(periodeFiscaleValeur.getDtFisc());
        anomalieF190Valeur.setDtDebPerFisc(periodeFiscaleValeur.getDtDebPerFisc());
        anomalieF190Valeur.setDtFinPerFisc(periodeFiscaleValeur.getDtFinPerFisc());
        anomalieF190Valeur.setRefElFBase(refElfBase);
        anomalieF190Valeur.setRefElFResultat(refElfResultat);
        anomalieF190Valeur.setCdBareme(descriptifBaremeValeur.getCdBareme());
        anomalieF190Valeur.setLbBareme(descriptifBaremeValeur.getLbBareme());
        
        return new AnomalieF190Erreur(anomalieF190Valeur);
    }
    
    public AnomalieF195Erreur construireAnomalieF195(PeriodeFiscaleValeur periodeFiscaleValeur, String refElfBase, String refElfResultat, DescriptifBaremeValeur descriptifBaremeValeur) throws TechDysfonctionnementErreur {
        AnomalieF195Valeur anomalieF195Valeur = new AnomalieF195Valeur();
        anomalieF195Valeur.setDtFisc(periodeFiscaleValeur.getDtFisc());
        anomalieF195Valeur.setDtDebPerFisc(periodeFiscaleValeur.getDtDebPerFisc());
        anomalieF195Valeur.setDtFinPerFisc(periodeFiscaleValeur.getDtFinPerFisc());
        anomalieF195Valeur.setRefElFBase(refElfBase);
        anomalieF195Valeur.setRefElFResultat(refElfResultat);
        anomalieF195Valeur.setCdBareme(descriptifBaremeValeur.getCdBareme());
        anomalieF195Valeur.setLbBareme(descriptifBaremeValeur.getLbBareme());
        
        return new AnomalieF195Erreur(anomalieF195Valeur);
    }
    
    public AnomalieF200Erreur construireAnomalieF200(PeriodeFiscaleValeur periodeFiscaleValeur, String refElfBase, String refElfResultat, DescriptifBaremeValeur descriptifBaremeValeur, Integer pNbDevVldtTranche) throws TechDysfonctionnementErreur {
        AnomalieF200Valeur anomalieF200Valeur = new AnomalieF200Valeur();
        anomalieF200Valeur.setDtFisc(periodeFiscaleValeur.getDtFisc());
        anomalieF200Valeur.setDtDebPerFisc(periodeFiscaleValeur.getDtDebPerFisc());
        anomalieF200Valeur.setDtFinPerFisc(periodeFiscaleValeur.getDtFinPerFisc());
        anomalieF200Valeur.setRefElFBase(refElfBase);
        anomalieF200Valeur.setRefElFResultat(refElfResultat);
        anomalieF200Valeur.setCdBareme(descriptifBaremeValeur.getCdBareme());
        anomalieF200Valeur.setLbBareme(descriptifBaremeValeur.getLbBareme());
        anomalieF200Valeur.setPNbDebVldtTranche(pNbDevVldtTranche);
        
        return new AnomalieF200Erreur(anomalieF200Valeur);
    }
    
    public AnomalieF205Erreur construireAnomalieF205(PeriodeFiscaleValeur periodeFiscaleValeur, String refElfBase, String refElfResultat, DescriptifBaremeValeur descriptifBaremeValeur) throws TechDysfonctionnementErreur {
        AnomalieF205Valeur anomalie = new AnomalieF205Valeur();
        
        anomalie.setDtFisc(periodeFiscaleValeur.getDtFisc());
        anomalie.setDtDebPerFisc(periodeFiscaleValeur.getDtDebPerFisc());
        anomalie.setDtFinPerFisc(periodeFiscaleValeur.getDtFinPerFisc());
        anomalie.setRefElF(refElfBase);
        anomalie.setRefElFResultat(refElfResultat);
        anomalie.setCdBareme(descriptifBaremeValeur.getCdBareme());
        anomalie.setLbBareme(descriptifBaremeValeur.getLbBareme());
        
        return new AnomalieF205Erreur(anomalie);
    }
    
    public AnomalieF210Erreur construireAnomalieF210(PeriodeFiscaleValeur periodeFiscaleValeur, String refElfBase, String refElfResultat, DescriptifBaremeValeur descriptifBaremeValeur) throws TechDysfonctionnementErreur {
        AnomalieF210Valeur anomalieF210 = new AnomalieF210Valeur();
        anomalieF210.setDtFisc(periodeFiscaleValeur.getDtFisc());
        anomalieF210.setDtDebPerFisc(periodeFiscaleValeur.getDtDebPerFisc());
        anomalieF210.setDtFinPerFisc(periodeFiscaleValeur.getDtFinPerFisc());
        anomalieF210.setRefElFBase(refElfBase);
        anomalieF210.setRefElFResultat(refElfResultat);
        anomalieF210.setCdBareme(descriptifBaremeValeur.getCdBareme());
        anomalieF210.setLbBareme(descriptifBaremeValeur.getLbBareme());
        
        return new AnomalieF210Erreur(anomalieF210);
    }
    
    public AnomalieF220Erreur construireAnomalieF220(PeriodeFiscaleValeur periodeFiscaleValeur, String refElfBase, String refElfResultat, DescriptifBaremeValeur descriptifBaremeValeur, Integer[] vlBorneTranche) throws TechDysfonctionnementErreur {
        AnomalieF220Valeur anomalieF220Valeur = new AnomalieF220Valeur();
        anomalieF220Valeur.setDtFisc(periodeFiscaleValeur.getDtFisc());
        anomalieF220Valeur.setDtDebPerFisc(periodeFiscaleValeur.getDtDebPerFisc());
        anomalieF220Valeur.setDtFinPerFisc(periodeFiscaleValeur.getDtFinPerFisc());
        anomalieF220Valeur.setRefElFBase(refElfBase);
        anomalieF220Valeur.setRefElFResultat(refElfResultat);
        anomalieF220Valeur.setCdBareme(descriptifBaremeValeur.getCdBareme());
        anomalieF220Valeur.setLbBareme(descriptifBaremeValeur.getLbBareme());
        anomalieF220Valeur.setVlBorneTranche(vlBorneTranche);
        
        return new AnomalieF220Erreur(anomalieF220Valeur);
    }
    
    public AnomalieF225Erreur construireAnomalieF225(PeriodeFiscaleValeur periodeFiscaleValeur, String refElfBase, String refElfResultat, DescriptifBaremeValeur descriptifBaremeValeur, Integer[] vlBorneTranche, Integer[] pNbValTranche)
            throws TechDysfonctionnementErreur {
        AnomalieF225Valeur anomalie = new AnomalieF225Valeur();
        
        anomalie.setDtFisc(periodeFiscaleValeur.getDtFisc());
        anomalie.setDtDebPerFisc(periodeFiscaleValeur.getDtDebPerFisc());
        anomalie.setDtFinPerFisc(periodeFiscaleValeur.getDtFinPerFisc());
        anomalie.setRefElFBase(refElfBase);
        anomalie.setRefElFResultat(refElfResultat);
        anomalie.setCdBareme(descriptifBaremeValeur.getCdBareme());
        anomalie.setLbBareme(descriptifBaremeValeur.getLbBareme());
        anomalie.setVlBorneTranche(vlBorneTranche);
        anomalie.setNbValTranche(pNbValTranche);
        
        return new AnomalieF225Erreur(anomalie);
    }
    
    public AnomalieValeur[] consoliderAnomalies(ListeAnomaliesErreur listeAno) throws TechDysfonctionnementErreur {
        AnomalieValeur[] retour = null;
        
        if (listeAno != null) {
            
            AnomalieErreur[] erreurs = listeAno.getListeAnomalieErreur();
            retour = new AnomalieValeur[erreurs.length];
            
            for (int i = 0; i < erreurs.length; i++) {
                if (erreurs[i].getAnomalieValeur() != null) {
                    String libelle = constructionAnomalieDaoEntite.construireAnomalie(erreurs[i].getAnomalieValeur().getCode());
                    erreurs[i].getAnomalieValeur().setLibelle(libelle);
                    retour[i] = erreurs[i].getAnomalieValeur();
                }
            }
        }
        
        return retour;
    }
}