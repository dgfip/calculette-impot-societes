/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.domaine;

import java.util.ArrayList;
import java.util.List;

import fr.gouv.impots.appli.capromas.transverse.RetourTraitementValeur;
import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieErreur;
import fr.gouv.impots.appli.capromas.transverse.udd.metier.ElementFiscalCalculeValeur;

public final class RetourTraitementUtil {
    
    private transient List<AnomalieErreur> listeAnomaliesErreurs;
    
    private final transient List<ElementFiscalCalculeValeur> listeElementsFiscaux;
    
    public RetourTraitementUtil() {
        super();
        listeElementsFiscaux = new ArrayList<ElementFiscalCalculeValeur>();
        listeAnomaliesErreurs = new ArrayList<AnomalieErreur>();
    }
    
    public ElementFiscalCalculeValeur[] getElementsFiscauxCalcule() {
        ElementFiscalCalculeValeur[] listeElem = (ElementFiscalCalculeValeur[]) this.listeElementsFiscaux.toArray(new ElementFiscalCalculeValeur[this.listeElementsFiscaux.size()]);
        return listeElem;
    }
    
    public void ajouterRetourTraitementValeur(RetourTraitementValeur retourTraitement) {
        if (retourTraitement != null) {
            this.ajouterElementsFiscauxCalcules(retourTraitement.getListeElementFiscalCalculeValeur());
            this.ajouterAnomalies(retourTraitement.getListeAnomalieErreur());
        }
    }
    
    public void ajouterElementsFiscauxCalcules(ElementFiscalCalculeValeur[] listeEF) {
        if (listeEF != null) {
            for (int i = 0; i < listeEF.length; i++) {
                this.ajouterElementFiscalCalcule(listeEF[i]);
            }
        }
    }
    
    public void ajouterElementFiscalCalcule(ElementFiscalCalculeValeur elem) {
        if (elem == null || this.listeElementsFiscaux.contains(elem)) {
            if (this.listeElementsFiscaux.contains(elem)) {
                this.listeElementsFiscaux.remove(elem);
                this.listeElementsFiscaux.add(elem);
            }
        } else {
            this.listeElementsFiscaux.add(elem);
        }
    }
    
    public void ajouterAnomalies(AnomalieErreur[] listeAnos) {
        if (listeAnos != null) {
            for (int i = 0; i < listeAnos.length; i++) {
                this.ajouterAnomalie(listeAnos[i]);
            }
        }
    }
    
    public void ajouterAnomalie(AnomalieErreur anomalie) {
        if (anomalie != null) {
            
            this.listeAnomaliesErreurs.add(anomalie);
        }
    }
    
    public RetourTraitementValeur genererRetourTraitementValeur() {
        RetourTraitementValeur retour = new RetourTraitementValeur();
        
        AnomalieErreur[] listeAnomalies = (AnomalieErreur[]) this.listeAnomaliesErreurs.toArray(new AnomalieErreur[this.listeAnomaliesErreurs.size()]);
        retour.setListeAnomalieErreur(listeAnomalies);
        
        ElementFiscalCalculeValeur[] listeElem = this.getElementsFiscauxCalcule();
        retour.setListeElementFiscalCalculeValeur(listeElem);
        
        return retour;
    }
}
