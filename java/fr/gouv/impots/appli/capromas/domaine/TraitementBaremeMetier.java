/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.domaine;

import fr.gouv.impots.appli.capromas.persistance.TraitementBaremeDaoEntite;
import fr.gouv.impots.appli.capromas.transverse.DonneesBareme;
import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieF051Erreur;
import fr.gouv.impots.appli.capromas.transverse.baremes.BaremeAttributaireValeur;
import fr.gouv.impots.appli.capromas.transverse.baremes.BaremeGeneralValeur;
import fr.gouv.impots.appli.capromas.transverse.baremes.BaremeLimiteValeur;
import fr.gouv.impots.appli.capromas.transverse.baremes.BaremeTatValeur;
import fr.gouv.impots.appli.capromas.transverse.baremes.BaremeTstValeur;
import fr.gouv.impots.appli.capromas.transverse.baremes.DescriptifBaremeValeur;
import fr.gouv.impots.appli.commun.frameworks.gestionerreurs.exceptions.TechDysfonctionnementErreur;
import fr.gouv.impots.appli.commun.frameworks.gestionerreurs.transverse.CodeErreurImpl;
import fr.gouv.impots.appli.commun.frameworks.gestionerreurs.transverse.GestionnaireErreurs;

public final class TraitementBaremeMetier {
    
    private TraitementBaremeDaoEntite daoBaremes = null;
    
    private transient ConstructionAnomalieMetier constructionAnomalieMetier;
    
    public TraitementBaremeMetier() {
        super();
    }
    
    public void setDaoBaremes(TraitementBaremeDaoEntite newDaoBaremes) {
        this.daoBaremes = newDaoBaremes;
    }
    
    public void setConstructionAnomalieMetier(ConstructionAnomalieMetier newConstructionAnomalieMetier) {
        this.constructionAnomalieMetier = newConstructionAnomalieMetier;
    }
    
    public DescriptifBaremeValeur rechercherCaracteristiques(String pCodeBareme) throws TechDysfonctionnementErreur, AnomalieF051Erreur {
        boolean codeBaremeExistant = false;
        
        DescriptifBaremeValeur descriptif = new DescriptifBaremeValeur();
        descriptif.setCdBareme(pCodeBareme);
        
        codeBaremeExistant = consulterBareme(descriptif);
        
        if (codeBaremeExistant) {
            determinerGeneralite(descriptif);
            
            if (descriptif.isOnGeneral()) {
                if (descriptif.getBaremeGeneral() == null) {
                    descriptif.setBaremeGeneral(new BaremeGeneralValeur());
                }
                
                determinerIndividualisation(descriptif);
                
                consulterBaremeGeneral(descriptif);
            } else {
                if (descriptif.getBaremeAttributaire() == null) {
                    descriptif.setBaremeAttributaire(new BaremeAttributaireValeur());
                }
                
                consulterBaremeAttributaire(descriptif);
                
                if (descriptif.getBaremeAttributaire().isOnClefRepartition()) {
                    descriptif.setTypeUtilite(DonneesBareme.TYPE_UTILITE_CLE);
                } else {
                    DescriptifBaremeValeur baremeRef = this.rechercherCaracteristiques(descriptif.getBaremeAttributaire().getCdBaremeRef());
                    descriptif.getBaremeAttributaire().setBaremeRef(baremeRef);
                }
            }
        } else {
            throw constructionAnomalieMetier.construireAnomalieF051(pCodeBareme);
        }
        
        return descriptif;
    }
    
    public boolean consulterBareme(DescriptifBaremeValeur pDescriptif) throws TechDysfonctionnementErreur {
        return daoBaremes.consulterBareme(pDescriptif);
    }
    
    public void determinerGeneralite(DescriptifBaremeValeur pDescriptif) throws TechDysfonctionnementErreur {
        daoBaremes.determinerGeneralite(pDescriptif);
    }
    
    public void determinerIndividualisation(DescriptifBaremeValeur pDescriptif) throws TechDysfonctionnementErreur {
        daoBaremes.determinerIndividualisation(pDescriptif);
    }
    
    public void consulterBaremeGeneral(DescriptifBaremeValeur pDescriptif) throws TechDysfonctionnementErreur {
        if (DonneesBareme.TYPE_UTILITE_TST.equals(pDescriptif.getTypeUtilite())) {
            
            if (pDescriptif.getBaremeGeneral().getBaremeTat() == null) {
                pDescriptif.getBaremeGeneral().setBaremeTst(new BaremeTstValeur());
            }
            
            daoBaremes.consulterBaremeTst(pDescriptif.getCdBareme(), pDescriptif.getBaremeGeneral());
        } else if (DonneesBareme.TYPE_UTILITE_TAT.equals(pDescriptif.getTypeUtilite())) {
            
            if (pDescriptif.getBaremeGeneral().getBaremeTat() == null) {
                pDescriptif.getBaremeGeneral().setBaremeTat(new BaremeTatValeur());
            }
            
            daoBaremes.consulterBaremeTat(pDescriptif.getCdBareme(), pDescriptif.getBaremeGeneral());
        } else if (DonneesBareme.TYPE_UTILITE_LIMITE.equals(pDescriptif.getTypeUtilite())) {
            
            if (pDescriptif.getBaremeGeneral().getBaremeLimite() == null) {
                pDescriptif.getBaremeGeneral().setBaremeLimite(new BaremeLimiteValeur());
            }
            
            daoBaremes.consulterBaremeLimite(pDescriptif.getCdBareme(), pDescriptif.getBaremeGeneral());
        } else {
            throw (TechDysfonctionnementErreur) GestionnaireErreurs.construireErreur(CodeErreurImpl.CODE_DYSF_RESSOURCES);
        }
    }
    
    public void consulterBaremeAttributaire(DescriptifBaremeValeur pDescriptif) throws TechDysfonctionnementErreur {
        daoBaremes.consulterBaremeAttributaire(pDescriptif);
    }
}
