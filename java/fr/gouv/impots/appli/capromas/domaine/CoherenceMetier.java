/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.domaine;

import java.util.Calendar;
import java.util.List;

import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieErreur;

public final class CoherenceMetier {
    
    private ConstructionAnomalieMetier constructionAnomalieMetier = null;
    
    public CoherenceMetier() {
        super();
    }
    
    public List<AnomalieErreur> verifierDateDansCalendrier(final Calendar date, List<AnomalieErreur> listeAno) {
        date.setLenient(false);
        
        try {
            date.getTime();
        } catch (IllegalArgumentException e) {
            date.setLenient(true);
            AnomalieErreur anomalie = constructionAnomalieMetier.construireAnomalieF007(date);
            listeAno.add(anomalie);
        }
        
        return listeAno;
    }
    
}
