/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.domaine;

import fr.gouv.impots.appli.capromas.transverse.udd.metier.ElementFiscalValeur;

public final class CleElementFiscal {
    
    private final transient String cdEFisc;
    
    private final transient Integer noDistinctionEf;
    
    public CleElementFiscal(final ElementFiscalValeur elementFiscal) {
        this.cdEFisc = elementFiscal.getCdEFisc();
        this.noDistinctionEf = elementFiscal.getNoDistinctionEf();
    }
    
    public CleElementFiscal(final String codeElementFiscal, final Integer noDistinction) {
        this.cdEFisc = codeElementFiscal;
        this.noDistinctionEf = noDistinction;
    }
    
    public boolean equals(Object obj) {
        boolean res = false;
        if (this == obj) {
            res = true;
        } else if (obj instanceof CleElementFiscal) {
            CleElementFiscal cleComparee = (CleElementFiscal) obj;
            
            if (cleComparee.getNoDistinctionEf() != null) {
                res = (cleComparee.getCdEFisc().equals(cdEFisc) && cleComparee.getNoDistinctionEf().equals(noDistinctionEf));
            }
            res = cleComparee.getCdEFisc().equals(cdEFisc);
        } else {
            res = false;
        }
        return res;
    }
    
    public int hashCode() {
        int res = 0;
        if (noDistinctionEf == null) {
            res = cdEFisc.hashCode();
        } else {
            res = cdEFisc.hashCode() + noDistinctionEf.hashCode();
        }
        return res;
    }
    
    public String getCdEFisc() {
        return cdEFisc;
    }
    
    public Integer getNoDistinctionEf() {
        return noDistinctionEf;
    }
}
