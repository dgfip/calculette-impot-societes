/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.domaine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import fr.gouv.impots.appli.capromas.transverse.udd.metier.ElementFiscalValeur;

public class TableElementFiscal extends HashMap<String, Map<CleElementFiscal, ElementFiscalValeur>> {
    
    private static final long serialVersionUID = 1084470357811354061L;
    
    public TableElementFiscal(final ElementFiscalValeur elementFiscal) {
        super();
        this.ajouterElementFiscal(elementFiscal);
    }
    
    public TableElementFiscal(final ElementFiscalValeur[] elementsFiscaux) {
        super();
        ajouterElementsFiscaux(elementsFiscaux);
    }
    
    public TableElementFiscal() {
        super();
    }
    
    public final void ajouterElementFiscal(ElementFiscalValeur elementFiscal) {
        if (elementFiscal != null) {
            CleElementFiscal cle = new CleElementFiscal(elementFiscal);
            
            if (super.containsKey(elementFiscal.getCdEFisc())) {
                super.get(elementFiscal.getCdEFisc()).put(cle, elementFiscal);
            } else {
                Map<CleElementFiscal, ElementFiscalValeur> tableElementFiscal = new HashMap<CleElementFiscal, ElementFiscalValeur>();
                tableElementFiscal.put(cle, elementFiscal);
                super.put(elementFiscal.getCdEFisc(), tableElementFiscal);
            }
        }
    }
    
    public final void ajouterElementsFiscaux(ElementFiscalValeur[] elementsFiscaux) {
        if (elementsFiscaux != null) {
            for (int i = elementsFiscaux.length - 1; i >= 0; i--) {
                ajouterElementFiscal(elementsFiscaux[i]);
            }
        }
    }
    
    public final ElementFiscalValeur retirerElementFiscal(String codeElementFiscal, Integer numeroDistinction) {
        ElementFiscalValeur elementFiscalRetire = null;
        
        Map<CleElementFiscal, ElementFiscalValeur> tableElementFiscal = super.get(codeElementFiscal);
        
        if (tableElementFiscal != null) {
            CleElementFiscal cle = new CleElementFiscal(codeElementFiscal, numeroDistinction);
            elementFiscalRetire = (ElementFiscalValeur) tableElementFiscal.remove(cle);
        }
        
        return elementFiscalRetire;
    }
    
    public final ElementFiscalValeur[] retirerElementsFiscaux(String codeElementFiscal) {
        ElementFiscalValeur[] retour = null;
        
        Map<CleElementFiscal, ElementFiscalValeur> tableElementFiscal = super.remove(codeElementFiscal);
        
        if (tableElementFiscal == null) {
            
            retour = new ElementFiscalValeur[0];
        } else {
            retour = new ElementFiscalValeur[tableElementFiscal.values().size()];
            retour = (ElementFiscalValeur[]) tableElementFiscal.values().toArray(retour);
        }
        
        return retour;
    }
    
    public final ElementFiscalValeur[] getElementsFiscaux(String codeElementFiscal) {
        ElementFiscalValeur[] retour = null;
        Map<CleElementFiscal, ElementFiscalValeur> tableElementFiscal = super.get(codeElementFiscal);
        
        if (tableElementFiscal != null) {
            retour = new ElementFiscalValeur[tableElementFiscal.values().size()];
            retour = (ElementFiscalValeur[]) tableElementFiscal.values().toArray(retour);
        }
        
        return retour;
    }
    
    public final ElementFiscalValeur getElementFiscalSimple(String codeElementFiscal) {
        ElementFiscalValeur res = null;
        ElementFiscalValeur[] retour = getElementsFiscaux(codeElementFiscal);
        
        if (retour != null && retour.length > 0) {
            res = retour[0];
        }
        return res;
    }
    
    public final ElementFiscalValeur getElementFiscal(String codeElementFiscal, Integer numeroDistinction) {
        ElementFiscalValeur retour = null;
        
        if ((ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI == numeroDistinction) || ((numeroDistinction != null) && numeroDistinction.equals(ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI))) {
            
            retour = this.getElementFiscalSimple(codeElementFiscal);
        } else {
            Map<CleElementFiscal, ElementFiscalValeur> tableElementFiscal = super.get(codeElementFiscal);
            
            if (tableElementFiscal != null) {
                retour = (ElementFiscalValeur) tableElementFiscal.get(new CleElementFiscal(codeElementFiscal, numeroDistinction));
            }
        }
        
        return retour;
    }
    
    public final ElementFiscalValeur[] getElementsFiscauxValeurs() {
        List<Object> listeRetour = new ArrayList<Object>();
        
        for (Iterator<String> itCles = super.keySet().iterator(); itCles.hasNext();) {
            Map<CleElementFiscal, ElementFiscalValeur> table = super.get(itCles.next());
            
            if (table != null) {
                for (Iterator<CleElementFiscal> itClesEltF = table.keySet().iterator(); itClesEltF.hasNext();) {
                    listeRetour.add(table.get(itClesEltF.next()));
                }
            }
        }
        
        ElementFiscalValeur[] tableauRetour = new ElementFiscalValeur[listeRetour.size()];
        
        return (ElementFiscalValeur[]) listeRetour.toArray(tableauRetour);
    }
    
    public final ElementFiscalValeur retirerElementFiscalSimple(String codeFiscal) {
        ElementFiscalValeur eltFiscalRetire = null;
        ElementFiscalValeur[] tabRetires = retirerElementsFiscaux(codeFiscal);
        
        if (tabRetires.length > 0) {
            eltFiscalRetire = tabRetires[0];
        }
        
        return eltFiscalRetire;
    }
}