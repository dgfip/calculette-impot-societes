/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.domaine;

import java.util.ArrayList;
import java.util.List;

import fr.gouv.impots.appli.capromas.domaine.is.CalculIs02Impl;
import fr.gouv.impots.appli.capromas.domaine.is.CalculIs03Impl;
import fr.gouv.impots.appli.capromas.domaine.is.CalculIs04Impl;
import fr.gouv.impots.appli.capromas.domaine.is.CalculIs05Impl;
import fr.gouv.impots.appli.capromas.domaine.is.CalculIs06Impl;
import fr.gouv.impots.appli.capromas.domaine.is.CalculIs11_1Impl;
import fr.gouv.impots.appli.capromas.domaine.is.CalculIs11_2Impl;
import fr.gouv.impots.appli.capromas.domaine.is.CalculIs11_4Impl;
import fr.gouv.impots.appli.capromas.domaine.is.CalculIs11_5Impl;
import fr.gouv.impots.appli.capromas.domaine.is.CalculIs11_6Impl;
import fr.gouv.impots.appli.capromas.domaine.is.CalculIs12Impl;
import fr.gouv.impots.appli.capromas.domaine.is.CalculIs13Impl;
import fr.gouv.impots.appli.capromas.domaine.is.CalculIs14Impl;
import fr.gouv.impots.appli.capromas.domaine.is.CalculIs15Impl;
import fr.gouv.impots.appli.capromas.domaine.is.CalculIs16Impl;
import fr.gouv.impots.appli.capromas.domaine.is.CalculIs17Impl;
import fr.gouv.impots.appli.capromas.domaine.is.CalculIs19Impl;
import fr.gouv.impots.appli.capromas.domaine.is.CalculIs20Impl;
import fr.gouv.impots.appli.capromas.domaine.is.CalculIs22Impl;
import fr.gouv.impots.appli.capromas.domaine.is.CalculIs23Impl;
import fr.gouv.impots.appli.capromas.domaine.is.CalculIs24Impl;
import fr.gouv.impots.appli.capromas.transverse.RetourTraitementValeur;
import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieErreur;
import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieF010Erreur;
import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieF050Erreur;
import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieF051Erreur;
import fr.gouv.impots.appli.capromas.transverse.anomalies.ListeAnomaliesErreur;
import fr.gouv.impots.appli.capromas.transverse.baremes.DescriptifBaremeValeur;
import fr.gouv.impots.appli.capromas.transverse.baremes.RechercheValeursBaremeReponseValeur;
import fr.gouv.impots.appli.capromas.transverse.baremes.RechercheValeursBaremeValeur;
import fr.gouv.impots.appli.capromas.transverse.udd.metier.ElementFiscalCalculeValeur;
import fr.gouv.impots.appli.capromas.transverse.udd.metier.ElementFiscalValeur;
import fr.gouv.impots.appli.capromas.transverse.udd.metier.PeriodeFiscaleValeur;
import fr.gouv.impots.appli.commun.frameworks.gestionerreurs.exceptions.TechDysfonctionnementErreur;

public class CalculFormulaire2572Metier extends CalculFormulaireParent {
    
    private final static String ISTXBENNORM = "ISTXBENNORM";
    
    private final static String ISTXBENPME = "ISTXBENPME";
    
    private final static String CSBTX = "CSBTX";
    
    private final static String CRLTX = "CRLTX";
    
    private final static String ISTXBENNORM2 = "ISTXBENNORM2";
    
    private RechercheValeurBaremeMetier rechercheValeurBaremeMetier;
    
    private TraitementBaremeMetier traitementBaremeMetier;
    
    private List<AnomalieErreur> listeAnomalies;
    
    public CalculFormulaire2572Metier() {
        super();
    }
    
    public RetourTraitementValeur traiter2572(TableElementFiscal table, PeriodeFiscaleValeur periode) throws TechDysfonctionnementErreur, ListeAnomaliesErreur {
        RetourTraitementValeur retour = new RetourTraitementValeur();
        
        RetourTraitementUtil retourUtil = new RetourTraitementUtil();
        listeAnomalies = new ArrayList<AnomalieErreur>();
        
        ElementFiscalValeur ef906443 = table.getElementFiscal(CodesElementsFiscaux.EF_906443, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef906576 = table.getElementFiscal(CodesElementsFiscaux.EF_906576, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        if ((ef906443 != null && ef906443.estValorise()) || (ef906576 != null && ef906576.estValorise())) {
            RechercheValeursBaremeReponseValeur reponseIsTxBenNorm = rechercherBareme(ISTXBENNORM, periode);
            if (reponseIsTxBenNorm == null) {
                retourUtil.ajouterAnomalies(listeAnomalies.toArray(new AnomalieErreur[listeAnomalies.size()]));
                return retourUtil.genererRetourTraitementValeur();
            }
            CalculIs11_1Impl calculIs11_1Impl = new CalculIs11_1Impl();
            
            ElementFiscalCalculeValeur[] retourIs11_1 = calculIs11_1Impl.valoriserEltsFiscaux(table.getElementsFiscauxValeurs(), reponseIsTxBenNorm.getTheValeursBaremeSansTrancheValeur().getTheValeurBaremeValeur()[0].getValeurBaremeTaux());
            retour.setListeElementFiscalCalculeValeur(retourIs11_1);
            retourUtil.ajouterRetourTraitementValeur(retour);
            retour = retourUtil.genererRetourTraitementValeur();
            
            for (int i = 0; i < retourIs11_1.length; i++) {
                table.ajouterElementFiscal(retourIs11_1[i]);
            }
        }
        
        ElementFiscalValeur ef909419 = table.getElementFiscal(CodesElementsFiscaux.EF_909419, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef909420 = table.getElementFiscal(CodesElementsFiscaux.EF_909420, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        if ((ef909420 != null && ef909420.estValorise()) || (ef909419 != null && ef909419.estValorise())) {
            
            RechercheValeursBaremeReponseValeur reponseIsTxBenNorm2 = rechercherBareme(ISTXBENNORM2, periode);
            if (reponseIsTxBenNorm2 == null) {
                retourUtil.ajouterAnomalies(listeAnomalies.toArray(new AnomalieErreur[listeAnomalies.size()]));
                return retourUtil.genererRetourTraitementValeur();
            }
            CalculIs11_6Impl calculIs11_6Impl = new CalculIs11_6Impl();
            
            ElementFiscalCalculeValeur[] retourIs11_6 = calculIs11_6Impl.valoriserEltsFiscaux(table.getElementsFiscauxValeurs(), reponseIsTxBenNorm2.getTheValeursBaremeSansTrancheValeur().getTheValeurBaremeValeur()[0].getValeurBaremeTaux());
            retour.setListeElementFiscalCalculeValeur(retourIs11_6);
            retourUtil.ajouterRetourTraitementValeur(retour);
            retour = retourUtil.genererRetourTraitementValeur();
            
            for (int i = 0; i < retourIs11_6.length; i++) {
                table.ajouterElementFiscal(retourIs11_6[i]);
            }
        }
        
        ElementFiscalValeur ef906444 = table.getElementFiscal(CodesElementsFiscaux.EF_906444, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef906577 = table.getElementFiscal(CodesElementsFiscaux.EF_906577, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        if ((ef906444 != null && ef906444.estValorise()) || (ef906577 != null && ef906577.estValorise())) {
            
            RechercheValeursBaremeReponseValeur reponseIsTxBenPme = rechercherBareme(ISTXBENPME, periode);
            if (reponseIsTxBenPme == null) {
                retourUtil.ajouterAnomalies(listeAnomalies.toArray(new AnomalieErreur[listeAnomalies.size()]));
                return retourUtil.genererRetourTraitementValeur();
            }
            CalculIs11_2Impl calculIs11_2Impl = new CalculIs11_2Impl();
            
            ElementFiscalCalculeValeur[] retourIs11_2 = calculIs11_2Impl.valoriserEltsFiscaux(table.getElementsFiscauxValeurs(), reponseIsTxBenPme.getTheValeursBaremeSansTrancheValeur().getTheValeurBaremeValeur()[0].getValeurBaremeTaux());
            retour.setListeElementFiscalCalculeValeur(retourIs11_2);
            retourUtil.ajouterRetourTraitementValeur(retour);
            retour = retourUtil.genererRetourTraitementValeur();
            
            for (int i = 0; i < retourIs11_2.length; i++) {
                table.ajouterElementFiscal(retourIs11_2[i]);
            }
        }
        
        ElementFiscalValeur ef902418 = table.getElementFiscal(CodesElementsFiscaux.EF_902418, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef902419 = table.getElementFiscal(CodesElementsFiscaux.EF_902419, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef500027 = table.getElementFiscal(CodesElementsFiscaux.EF_500027, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ef906576 = table.getElementFiscal(CodesElementsFiscaux.EF_906576, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ef906577 = table.getElementFiscal(CodesElementsFiscaux.EF_906577, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ef909420 = table.getElementFiscal(CodesElementsFiscaux.EF_909420, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        if ((ef902418 != null && ef902418.estValorise()) || (ef902419 != null && ef902419.estValorise()) || (ef906576 != null && ef906576.estValorise()) || (ef906577 != null && ef906577.estValorise()) || (ef500027 != null && ef500027.estValorise())
                || (ef909420 != null && ef909420.estValorise())) {
            
            CalculIs06Impl calculIs06Impl = new CalculIs06Impl();
            
            ElementFiscalCalculeValeur[] retourIs06 = calculIs06Impl.calculerTotalIsBrut(table.getElementsFiscauxValeurs());
            
            retour.setListeElementFiscalCalculeValeur(retourIs06);
            retourUtil.ajouterRetourTraitementValeur(retour);
            retour = retourUtil.genererRetourTraitementValeur();
            
            for (int i = 0; i < retourIs06.length; i++) {
                table.ajouterElementFiscal(retourIs06[i]);
            }
        }
        
        ElementFiscalValeur ef500028 = table.getElementFiscal(CodesElementsFiscaux.EF_500028, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef500029 = table.getElementFiscal(CodesElementsFiscaux.EF_500029, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef500048 = table.getElementFiscal(CodesElementsFiscaux.EF_500048, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef906449 = table.getElementFiscal(CodesElementsFiscaux.EF_906449, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef908549 = table.getElementFiscal(CodesElementsFiscaux.EF_908549, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        if ((ef500028 != null && ef500028.estValorise()) || (ef500029 != null && ef500029.estValorise()) || (ef500048 != null && ef500048.estValorise()) || (ef906449 != null && ef906449.estValorise())
                || (ef908549 != null && ef908549.estValorise())) {
            
            CalculIs12Impl calculIs12Impl = new CalculIs12Impl();
            
            ElementFiscalCalculeValeur[] retourIs12 = calculIs12Impl.calculerTotal(table.getElementsFiscauxValeurs());
            
            retour.setListeElementFiscalCalculeValeur(retourIs12);
            retourUtil.ajouterRetourTraitementValeur(retour);
            retour = retourUtil.genererRetourTraitementValeur();
            
            for (int i = 0; i < retourIs12.length; i++) {
                table.ajouterElementFiscal(retourIs12[i]);
            }
        }
        
        ElementFiscalValeur ef906450 = table.getElementFiscal(CodesElementsFiscaux.EF_906450, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ef500027 = table.getElementFiscal(CodesElementsFiscaux.EF_500027, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ef906449 = table.getElementFiscal(CodesElementsFiscaux.EF_906449, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        if ((ef500027 != null && ef500027.estValorise()) || (ef906449 != null && ef906449.estValorise()) || (ef906450 != null && ef906450.estValorise())) {
            
            CalculIs13Impl calculIs13Impl = new CalculIs13Impl();
            
            ElementFiscalCalculeValeur[] retourIs13 = calculIs13Impl.calculerMontant(table.getElementsFiscauxValeurs());
            
            retour.setListeElementFiscalCalculeValeur(retourIs13);
            retourUtil.ajouterRetourTraitementValeur(retour);
            retour = retourUtil.genererRetourTraitementValeur();
            
            for (int i = 0; i < retourIs13.length; i++) {
                table.ajouterElementFiscal(retourIs13[i]);
            }
        }
        
        ElementFiscalValeur ef500038 = table.getElementFiscal(CodesElementsFiscaux.EF_500038, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef906451 = table.getElementFiscal(CodesElementsFiscaux.EF_906451, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef904112 = table.getElementFiscal(CodesElementsFiscaux.EF_904112, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef904111 = table.getElementFiscal(CodesElementsFiscaux.EF_904111, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef906455 = table.getElementFiscal(CodesElementsFiscaux.EF_906455, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef500036 = table.getElementFiscal(CodesElementsFiscaux.EF_500036, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef906452 = table.getElementFiscal(CodesElementsFiscaux.EF_906452, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef906453 = table.getElementFiscal(CodesElementsFiscaux.EF_906453, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef500034 = table.getElementFiscal(CodesElementsFiscaux.EF_500034, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef906454 = table.getElementFiscal(CodesElementsFiscaux.EF_906454, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef500054 = table.getElementFiscal(CodesElementsFiscaux.EF_500054, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef906456 = table.getElementFiscal(CodesElementsFiscaux.EF_906456, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef907017 = table.getElementFiscal(CodesElementsFiscaux.EF_907017, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef907018 = table.getElementFiscal(CodesElementsFiscaux.EF_907018, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        if ((ef500038 != null && ef500038.estValorise()) || (ef906451 != null && ef906451.estValorise()) || (ef904112 != null && ef904112.estValorise()) || (ef904111 != null && ef904111.estValorise()) || (ef906455 != null && ef906455.estValorise())
                || (ef500036 != null && ef500036.estValorise()) || (ef906452 != null && ef906452.estValorise()) || (ef906453 != null && ef906453.estValorise()) || (ef500034 != null && ef500034.estValorise())
                || (ef906454 != null && ef906454.estValorise()) || (ef500054 != null && ef500054.estValorise()) || (ef906456 != null && ef906456.estValorise()) || (ef907017 != null && ef907017.estValorise())
                || (ef907018 != null && ef907018.estValorise())) {
            
            CalculIs14Impl calculIs14Impl = new CalculIs14Impl();
            
            ElementFiscalCalculeValeur[] retourIs14 = calculIs14Impl.calculerTotal(table.getElementsFiscauxValeurs());
            
            retour.setListeElementFiscalCalculeValeur(retourIs14);
            retourUtil.ajouterRetourTraitementValeur(retour);
            retour = retourUtil.genererRetourTraitementValeur();
            
            for (int i = 0; i < retourIs14.length; i++) {
                table.ajouterElementFiscal(retourIs14[i]);
            }
        }
        
        ElementFiscalValeur ef906457 = table.getElementFiscal(CodesElementsFiscaux.EF_906457, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ef906450 = table.getElementFiscal(CodesElementsFiscaux.EF_906450, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ef906456 = table.getElementFiscal(CodesElementsFiscaux.EF_906456, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        if ((ef906450 != null && ef906450.estValorise()) || (ef906456 != null && ef906456.estValorise()) || (ef906457 != null && ef906457.estValorise())) {
            
            CalculIs15Impl calculIs15Impl = new CalculIs15Impl();
            
            ElementFiscalCalculeValeur[] retourIs15 = calculIs15Impl.calculerMontant(table.getElementsFiscauxValeurs());
            
            retour.setListeElementFiscalCalculeValeur(retourIs15);
            retourUtil.ajouterRetourTraitementValeur(retour);
            retour = retourUtil.genererRetourTraitementValeur();
            
            for (int i = 0; i < retourIs15.length; i++) {
                table.ajouterElementFiscal(retourIs15[i]);
            }
        }
        
        ElementFiscalValeur ef500031 = table.getElementFiscal(CodesElementsFiscaux.EF_500031, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef500032 = table.getElementFiscal(CodesElementsFiscaux.EF_500032, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef500033 = table.getElementFiscal(CodesElementsFiscaux.EF_500033, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef500042 = table.getElementFiscal(CodesElementsFiscaux.EF_500042, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef500043 = table.getElementFiscal(CodesElementsFiscaux.EF_500043, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef500047 = table.getElementFiscal(CodesElementsFiscaux.EF_500047, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef500049 = table.getElementFiscal(CodesElementsFiscaux.EF_500049, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef500052 = table.getElementFiscal(CodesElementsFiscaux.EF_500052, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef900763 = table.getElementFiscal(CodesElementsFiscaux.EF_900763, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef900764 = table.getElementFiscal(CodesElementsFiscaux.EF_900764, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef900767 = table.getElementFiscal(CodesElementsFiscaux.EF_900767, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef900769 = table.getElementFiscal(CodesElementsFiscaux.EF_900769, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef900771 = table.getElementFiscal(CodesElementsFiscaux.EF_900771, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef900772 = table.getElementFiscal(CodesElementsFiscaux.EF_900772, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef902447 = table.getElementFiscal(CodesElementsFiscaux.EF_902447, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef500053 = table.getElementFiscal(CodesElementsFiscaux.EF_500053, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef906459 = table.getElementFiscal(CodesElementsFiscaux.EF_906459, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef904110 = table.getElementFiscal(CodesElementsFiscaux.EF_904110, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef906507 = table.getElementFiscal(CodesElementsFiscaux.EF_906507, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef906508 = table.getElementFiscal(CodesElementsFiscaux.EF_906508, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef906506 = table.getElementFiscal(CodesElementsFiscaux.EF_906506, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef908550 = table.getElementFiscal(CodesElementsFiscaux.EF_908550, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        if ((ef500031 != null && ef500031.estValorise()) || (ef500032 != null && ef500032.estValorise()) || (ef500033 != null && ef500033.estValorise()) || (ef500042 != null && ef500042.estValorise()) || (ef500043 != null && ef500043.estValorise())
                || (ef500047 != null && ef500047.estValorise()) || (ef500049 != null && ef500049.estValorise()) || (ef500052 != null && ef500052.estValorise()) || (ef900763 != null && ef900763.estValorise())
                || (ef900764 != null && ef900764.estValorise()) || (ef900767 != null && ef900767.estValorise()) || (ef900769 != null && ef900769.estValorise()) || (ef900771 != null && ef900771.estValorise())
                || (ef900772 != null && ef900772.estValorise()) || (ef902447 != null && ef902447.estValorise()) || (ef500053 != null && ef500053.estValorise()) || (ef906459 != null && ef906459.estValorise())
                || (ef904110 != null && ef904110.estValorise()) || (ef906507 != null && ef906507.estValorise()) || (ef906508 != null && ef906508.estValorise()) || (ef906506 != null && ef906506.estValorise())
                || (ef908550 != null && ef908550.estValorise())) {
            
            CalculIs16Impl calculIs16Impl = new CalculIs16Impl();
            
            ElementFiscalCalculeValeur[] retourIs16 = calculIs16Impl.calculerTotal(table.getElementsFiscauxValeurs());
            
            retour.setListeElementFiscalCalculeValeur(retourIs16);
            retourUtil.ajouterRetourTraitementValeur(retour);
            retour = retourUtil.genererRetourTraitementValeur();
            
            for (int i = 0; i < retourIs16.length; i++) {
                table.ajouterElementFiscal(retourIs16[i]);
            }
        }
        
        ElementFiscalValeur ef906460 = table.getElementFiscal(CodesElementsFiscaux.EF_906460, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ef906457 = table.getElementFiscal(CodesElementsFiscaux.EF_906457, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ef906459 = table.getElementFiscal(CodesElementsFiscaux.EF_906459, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        if ((ef906457 != null && ef906457.estValorise()) || (ef906459 != null && ef906459.estValorise()) || (ef906460 != null && ef906460.estValorise())) {
            
            CalculIs17Impl calculIs17Impl = new CalculIs17Impl();
            
            ElementFiscalCalculeValeur[] retourIs17 = calculIs17Impl.calculerTotal(table.getElementsFiscauxValeurs());
            
            retour.setListeElementFiscalCalculeValeur(retourIs17);
            retourUtil.ajouterRetourTraitementValeur(retour);
            retour = retourUtil.genererRetourTraitementValeur();
            
            for (int i = 0; i < retourIs17.length; i++) {
                table.ajouterElementFiscal(retourIs17[i]);
            }
        }
        
        ElementFiscalValeur ef906475 = table.getElementFiscal(CodesElementsFiscaux.EF_906475, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef906504 = table.getElementFiscal(CodesElementsFiscaux.EF_906504, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        if ((ef906475 != null && ef906475.estValorise()) || (ef906504 != null && ef906504.estValorise())) {
            
            RechercheValeursBaremeReponseValeur reponseCsbTx = rechercherBareme(CSBTX, periode);
            if (reponseCsbTx == null) {
                retourUtil.ajouterAnomalies(listeAnomalies.toArray(new AnomalieErreur[listeAnomalies.size()]));
                return retourUtil.genererRetourTraitementValeur();
            }
            CalculIs11_4Impl calculIs11_4Impl = new CalculIs11_4Impl();
            
            ElementFiscalCalculeValeur[] retourIs11_4 = calculIs11_4Impl.valoriserEltsFiscaux(table.getElementsFiscauxValeurs(), reponseCsbTx.getTheValeursBaremeSansTrancheValeur().getTheValeurBaremeValeur()[0].getValeurBaremeTaux());
            retour.setListeElementFiscalCalculeValeur(retourIs11_4);
            retourUtil.ajouterRetourTraitementValeur(retour);
            retour = retourUtil.genererRetourTraitementValeur();
            
            for (int i = 0; i < retourIs11_4.length; i++) {
                table.ajouterElementFiscal(retourIs11_4[i]);
            }
        }
        
        ElementFiscalValeur ef500030 = table.getElementFiscal(CodesElementsFiscaux.EF_500030, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef906464 = table.getElementFiscal(CodesElementsFiscaux.EF_906464, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ef906504 = table.getElementFiscal(CodesElementsFiscaux.EF_906504, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        if ((ef906504 != null && ef906504.estValorise()) || (ef500030 != null && ef500030.estValorise()) || (ef906464 != null && ef906464.estValorise())) {
            
            CalculIs19Impl calculIs19Impl = new CalculIs19Impl();
            
            ElementFiscalCalculeValeur[] retourIs19 = calculIs19Impl.calculerTotal(table.getElementsFiscauxValeurs());
            
            retour.setListeElementFiscalCalculeValeur(retourIs19);
            retourUtil.ajouterRetourTraitementValeur(retour);
            retour = retourUtil.genererRetourTraitementValeur();
            
            for (int i = 0; i < retourIs19.length; i++) {
                table.ajouterElementFiscal(retourIs19[i]);
            }
        }
        
        ElementFiscalValeur ef906466 = table.getElementFiscal(CodesElementsFiscaux.EF_906466, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef906565 = table.getElementFiscal(CodesElementsFiscaux.EF_906565, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        if ((ef906466 != null && ef906466.estValorise()) || (ef906565 != null && ef906565.estValorise())) {
            
            RechercheValeursBaremeReponseValeur reponseCrlTx = rechercherBareme(CRLTX, periode);
            if (reponseCrlTx == null) {
                retourUtil.ajouterAnomalies(listeAnomalies.toArray(new AnomalieErreur[listeAnomalies.size()]));
                return retourUtil.genererRetourTraitementValeur();
            }
            CalculIs11_5Impl calculIs11_5Impl = new CalculIs11_5Impl();
            
            ElementFiscalCalculeValeur[] retourIs11_5 = calculIs11_5Impl.valoriserEltsFiscaux(table.getElementsFiscauxValeurs(), reponseCrlTx.getTheValeursBaremeSansTrancheValeur().getTheValeurBaremeValeur()[0].getValeurBaremeTaux());
            retour.setListeElementFiscalCalculeValeur(retourIs11_5);
            retourUtil.ajouterRetourTraitementValeur(retour);
            retour = retourUtil.genererRetourTraitementValeur();
            
            for (int i = 0; i < retourIs11_5.length; i++) {
                table.ajouterElementFiscal(retourIs11_5[i]);
            }
        }
        
        ElementFiscalValeur ef906461 = table.getElementFiscal(CodesElementsFiscaux.EF_906461, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef906566 = table.getElementFiscal(CodesElementsFiscaux.EF_906566, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef906567 = table.getElementFiscal(CodesElementsFiscaux.EF_906567, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ef906460 = table.getElementFiscal(CodesElementsFiscaux.EF_906460, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        if ((ef906460 != null && ef906460.estValorise()) || (ef906461 != null && ef906461.estValorise()) || (ef906566 != null && ef906566.estValorise()) || (ef906567 != null && ef906567.estValorise())) {
            
            CalculIs20Impl calculIs20Impl = new CalculIs20Impl();
            
            ElementFiscalCalculeValeur[] retourIs20 = calculIs20Impl.calculerTotal(table.getElementsFiscauxValeurs());
            
            retour.setListeElementFiscalCalculeValeur(retourIs20);
            retourUtil.ajouterRetourTraitementValeur(retour);
            retour = retourUtil.genererRetourTraitementValeur();
            
            for (int i = 0; i < retourIs20.length; i++) {
                table.ajouterElementFiscal(retourIs20[i]);
            }
        }
        
        ElementFiscalValeur ef906465 = table.getElementFiscal(CodesElementsFiscaux.EF_906465, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef906570 = table.getElementFiscal(CodesElementsFiscaux.EF_906570, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef906571 = table.getElementFiscal(CodesElementsFiscaux.EF_906571, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ef906464 = table.getElementFiscal(CodesElementsFiscaux.EF_906464, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        if ((ef906464 != null && ef906464.estValorise()) || (ef906465 != null && ef906465.estValorise()) || (ef906570 != null && ef906570.estValorise()) || (ef906571 != null && ef906571.estValorise())) {
            
            CalculIs22Impl calculIs22Impl = new CalculIs22Impl();
            
            ElementFiscalCalculeValeur[] retourIs22 = calculIs22Impl.calculerTotal(table.getElementsFiscauxValeurs());
            
            retour.setListeElementFiscalCalculeValeur(retourIs22);
            retourUtil.ajouterRetourTraitementValeur(retour);
            retour = retourUtil.genererRetourTraitementValeur();
            
            for (int i = 0; i < retourIs22.length; i++) {
                table.ajouterElementFiscal(retourIs22[i]);
            }
        }
        
        ElementFiscalValeur ef906467 = table.getElementFiscal(CodesElementsFiscaux.EF_906467, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef906572 = table.getElementFiscal(CodesElementsFiscaux.EF_906572, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef906573 = table.getElementFiscal(CodesElementsFiscaux.EF_906573, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ef906565 = table.getElementFiscal(CodesElementsFiscaux.EF_906565, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        if ((ef906565 != null && ef906565.estValorise()) || (ef906467 != null && ef906467.estValorise()) || (ef906572 != null && ef906572.estValorise()) || (ef906573 != null && ef906573.estValorise())) {
            
            CalculIs23Impl calculIs23Impl = new CalculIs23Impl();
            
            ElementFiscalCalculeValeur[] retourIs23 = calculIs23Impl.calculerTotal(table.getElementsFiscauxValeurs());
            
            retour.setListeElementFiscalCalculeValeur(retourIs23);
            retourUtil.ajouterRetourTraitementValeur(retour);
            retour = retourUtil.genererRetourTraitementValeur();
            
            for (int i = 0; i < retourIs23.length; i++) {
                table.ajouterElementFiscal(retourIs23[i]);
            }
        }
        
        ElementFiscalValeur ef500021 = table.getElementFiscal(CodesElementsFiscaux.EF_500021, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ef906566 = table.getElementFiscal(CodesElementsFiscaux.EF_906566, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ef906570 = table.getElementFiscal(CodesElementsFiscaux.EF_906570, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ef906572 = table.getElementFiscal(CodesElementsFiscaux.EF_906572, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        if ((ef906566 != null && ef906566.estValorise()) || (ef906570 != null && ef906570.estValorise()) || (ef906572 != null && ef906572.estValorise()) || (ef500021 != null && ef500021.estValorise())) {
            
            CalculIs02Impl calculIs02Impl = new CalculIs02Impl();
            
            ElementFiscalCalculeValeur[] retourIs02 = calculIs02Impl.calculerMontantRestantAPayer(table.getElementsFiscauxValeurs());
            
            retour.setListeElementFiscalCalculeValeur(retourIs02);
            retourUtil.ajouterRetourTraitementValeur(retour);
            retour = retourUtil.genererRetourTraitementValeur();
            
            for (int i = 0; i < retourIs02.length; i++) {
                table.ajouterElementFiscal(retourIs02[i]);
            }
        }
        
        ElementFiscalValeur ef500022 = table.getElementFiscal(CodesElementsFiscaux.EF_500022, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ef906567 = table.getElementFiscal(CodesElementsFiscaux.EF_906567, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ef906571 = table.getElementFiscal(CodesElementsFiscaux.EF_906571, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ef906573 = table.getElementFiscal(CodesElementsFiscaux.EF_906573, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        if ((ef906567 != null && ef906567.estValorise()) || (ef906571 != null && ef906571.estValorise()) || (ef906573 != null && ef906573.estValorise()) || (ef500022 != null && ef500022.estValorise())) {
            
            CalculIs03Impl calculIs03Impl = new CalculIs03Impl();
            
            ElementFiscalCalculeValeur[] retourIs03 = calculIs03Impl.calculerExcedentsEtContributionsAssimilees(table.getElementsFiscauxValeurs());
            
            retour.setListeElementFiscalCalculeValeur(retourIs03);
            retourUtil.ajouterRetourTraitementValeur(retour);
            retour = retourUtil.genererRetourTraitementValeur();
            
            for (int i = 0; i < retourIs03.length; i++) {
                table.ajouterElementFiscal(retourIs03[i]);
            }
        }
        
        ElementFiscalValeur ef500023 = table.getElementFiscal(CodesElementsFiscaux.EF_500023, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef500024 = table.getElementFiscal(CodesElementsFiscaux.EF_500024, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ef500021 = table.getElementFiscal(CodesElementsFiscaux.EF_500021, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ef500022 = table.getElementFiscal(CodesElementsFiscaux.EF_500022, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        if ((ef500021 != null && ef500021.estValorise()) || (ef500022 != null && ef500022.estValorise()) || (ef500023 != null && ef500023.estValorise()) || (ef500024 != null && ef500024.estValorise())) {
            
            CalculIs04Impl calculIs04Impl = new CalculIs04Impl();
            
            ElementFiscalCalculeValeur[] retourIs04 = calculIs04Impl.calculerMontantTotal(table.getElementsFiscauxValeurs());
            
            retour.setListeElementFiscalCalculeValeur(retourIs04);
            retourUtil.ajouterRetourTraitementValeur(retour);
            retour = retourUtil.genererRetourTraitementValeur();
            
            for (int i = 0; i < retourIs04.length; i++) {
                table.ajouterElementFiscal(retourIs04[i]);
            }
        }
        
        ElementFiscalValeur ef906473 = table.getElementFiscal(CodesElementsFiscaux.EF_906473, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef906564 = table.getElementFiscal(CodesElementsFiscaux.EF_906564, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ef500023 = table.getElementFiscal(CodesElementsFiscaux.EF_500023, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        if ((ef500023 != null && ef500023.estValorise()) || (ef906473 != null && ef906473.estValorise()) || (ef906564 != null && ef906564.estValorise())) {
            
            CalculIs24Impl calculIs24Impl = new CalculIs24Impl();
            
            ElementFiscalCalculeValeur[] retourIs24 = calculIs24Impl.calculerTotal(table.getElementsFiscauxValeurs());
            
            retour.setListeElementFiscalCalculeValeur(retourIs24);
            retourUtil.ajouterRetourTraitementValeur(retour);
            retour = retourUtil.genererRetourTraitementValeur();
            
            for (int i = 0; i < retourIs24.length; i++) {
                table.ajouterElementFiscal(retourIs24[i]);
            }
        }
        
        ElementFiscalValeur ef500025 = table.getElementFiscal(CodesElementsFiscaux.EF_500025, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef906470 = table.getElementFiscal(CodesElementsFiscaux.EF_906470, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ElementFiscalValeur ef500026 = table.getElementFiscal(CodesElementsFiscaux.EF_500026, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        ef500024 = table.getElementFiscal(CodesElementsFiscaux.EF_500024, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
        if ((ef500024 != null && ef500024.estValorise()) || (ef500025 != null && ef500025.estValorise()) || (ef906470 != null && ef906470.estValorise()) || (ef500026 != null && ef500026.estValorise())) {
            
            CalculIs05Impl calculIs05Impl = new CalculIs05Impl();
            
            ElementFiscalCalculeValeur[] retourIs05 = calculIs05Impl.calculerMontantTotalExcedent(table.getElementsFiscauxValeurs());
            
            retour.setListeElementFiscalCalculeValeur(retourIs05);
            retourUtil.ajouterRetourTraitementValeur(retour);
            retour = retourUtil.genererRetourTraitementValeur();
            
            for (int i = 0; i < retourIs05.length; i++) {
                table.ajouterElementFiscal(retourIs05[i]);
            }
        }
        
        return retour;
    }
    
    public void setRechercheValeurBaremeMetier(RechercheValeurBaremeMetier rechercheValeurBaremeMetier) {
        this.rechercheValeurBaremeMetier = rechercheValeurBaremeMetier;
    }
    
    public RechercheValeurBaremeMetier getRechercheValeurBaremeMetier() {
        return this.rechercheValeurBaremeMetier;
    }
    
    public void setTraitementBaremeMetier(TraitementBaremeMetier traitementBaremeMetier) {
        this.traitementBaremeMetier = traitementBaremeMetier;
    }
    
    public TraitementBaremeMetier getTraitementBaremeMetier() {
        return this.traitementBaremeMetier;
    }
    
    private RechercheValeursBaremeReponseValeur rechercherBareme(String codeBareme, PeriodeFiscaleValeur periode) throws TechDysfonctionnementErreur, ListeAnomaliesErreur {
        
        RetourTraitementUtil retourTraitementUtilitaire = new RetourTraitementUtil();
        
        RechercheValeursBaremeValeur criteres = new RechercheValeursBaremeValeur();
        
        if (periode.getDtFisc() == null) {
            criteres.setDateLeg1(periode.getDtDebPerFisc());
            criteres.setDateLeg2(periode.getDtFinPerFisc());
        } else {
            criteres.setDateLeg1(periode.getDtFisc());
        }
        
        RechercheValeursBaremeReponseValeur reponseRecherche = null;
        criteres.setCdBareme(codeBareme);
        
        try {
            reponseRecherche = rechercheValeurBaremeMetier.rechercherValeurApplicable(criteres);
        }
        
        catch (ListeAnomaliesErreur e) {
            retourTraitementUtilitaire.ajouterAnomalies(e.getListeAnomalieErreur());
        }
        
        DescriptifBaremeValeur descriptifBareme = null;
        try {
            descriptifBareme = traitementBaremeMetier.rechercherCaracteristiques(codeBareme);
        } catch (AnomalieF051Erreur e) {
            descriptifBareme = new DescriptifBaremeValeur();
            descriptifBareme.setCdBareme(codeBareme);
        }
        
        if ((reponseRecherche != null) && (reponseRecherche.getTheValeursBaremeSansTrancheValeur().getTheValeurBaremeValeur() != null) && (reponseRecherche.getTheValeursBaremeSansTrancheValeur().getTheValeurBaremeValeur().length == 1)) {
            return reponseRecherche;
        } else {
            
            if ((reponseRecherche != null) && (reponseRecherche.getTheValeursBaremeSansTrancheValeur().getTheValeurBaremeValeur() != null) && (reponseRecherche.getTheValeursBaremeSansTrancheValeur().getTheValeurBaremeValeur().length > 1)) {
                AnomalieF010Erreur anoF010 = this.getConstructeurAnomalies().construireAnomalieF010(periode, null, null, descriptifBareme, Integer.valueOf(reponseRecherche.getTheValeursBaremeSansTrancheValeur().getTheValeurBaremeValeur().length));
                retourTraitementUtilitaire.ajouterAnomalie(anoF010);
                listeAnomalies.add(anoF010);
                return null;
            } else {
                
                AnomalieF050Erreur anoF050 = this.getConstructeurAnomalies().construireAnomalieF050(periode, null, null, descriptifBareme);
                retourTraitementUtilitaire.ajouterAnomalie(anoF050);
                listeAnomalies.add(anoF050);
                return null;
            }
        }
    }
}