/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.domaine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import fr.gouv.impots.appli.capromas.persistance.RechercheValeurBaremeDaoEntite;
import fr.gouv.impots.appli.capromas.transverse.DonneesBareme;
import fr.gouv.impots.appli.capromas.transverse.ValeurBaremeValeur;
import fr.gouv.impots.appli.capromas.transverse.ValeursBaremeSansTrancheValeur;
import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieErreur;
import fr.gouv.impots.appli.capromas.transverse.anomalies.AnomalieF051Erreur;
import fr.gouv.impots.appli.capromas.transverse.anomalies.ListeAnomaliesErreur;
import fr.gouv.impots.appli.capromas.transverse.baremes.BaremeTatValeur;
import fr.gouv.impots.appli.capromas.transverse.baremes.DescriptifBaremeValeur;
import fr.gouv.impots.appli.capromas.transverse.baremes.RechercheValeursBaremeReponseValeur;
import fr.gouv.impots.appli.capromas.transverse.baremes.RechercheValeursBaremeValeur;
import fr.gouv.impots.appli.capromas.transverse.udd.anomalies.AnomalieValeur;
import fr.gouv.impots.appli.commun.frameworks.gestionerreurs.exceptions.TechDysfonctionnementErreur;

public final class RechercheValeurBaremeMetier {
    
    private RechercheValeurBaremeDaoEntite daoValeurs;
    
    private TraitementBaremeMetier traitementBaremeMetier;
    
    private transient CoherenceMetier coherenceMetier = null;
    
    private transient ConstructionAnomalieMetier constructionAnomalieMetier = null;
    
    public RechercheValeurBaremeMetier() {
        super();
    }
    
    public void setTraitementBaremeMetier(TraitementBaremeMetier newTraitementBaremeMetier) {
        this.traitementBaremeMetier = newTraitementBaremeMetier;
    }
    
    public void setDaoValeurs(RechercheValeurBaremeDaoEntite newDaoValeurs) {
        this.daoValeurs = newDaoValeurs;
    }
    
    public void setCoherenceMetier(CoherenceMetier newCoherenceMetier) {
        this.coherenceMetier = newCoherenceMetier;
    }
    
    public void setConstructionAnomalieMetier(ConstructionAnomalieMetier newConstructionAnomalieMetier) {
        this.constructionAnomalieMetier = newConstructionAnomalieMetier;
    }
    
    public RechercheValeursBaremeReponseValeur rechercherValeurApplicable(RechercheValeursBaremeValeur pCriteres) throws TechDysfonctionnementErreur, ListeAnomaliesErreur {
        
        RechercheValeursBaremeReponseValeur retourMethode = new RechercheValeursBaremeReponseValeur();
        
        List<AnomalieErreur> listeAnomalies = new ArrayList<AnomalieErreur>();
        
        try {
            controlerParametresAppel(pCriteres);
        } catch (ListeAnomaliesErreur e1) {
            listeAnomalies.addAll(Arrays.asList(e1.getListeAnomalieErreur()));
        }
        
        DescriptifBaremeValeur descriptif = null;
        
        try {
            descriptif = this.traitementBaremeMetier.rechercherCaracteristiques(pCriteres.getCdBareme());
        } catch (AnomalieF051Erreur e) {
            listeAnomalies.add(e);
        }
        
        if (listeAnomalies.size() > 0) {
            ListeAnomaliesErreur listeErreurs = new ListeAnomaliesErreur();
            
            AnomalieErreur[] listeAnos = new AnomalieErreur[0];
            
            listeErreurs.setListeAnomalieErreur((AnomalieErreur[]) listeAnomalies.toArray(listeAnos));
            
            throw listeErreurs;
        }
        
        if (descriptif != null) {
            
            if (DonneesBareme.TYPE_UTILITE_TAT.equals(descriptif.getTypeUtilite())) {
                BaremeTatValeur valeurATranche = new BaremeTatValeur();
                valeurATranche.setOnBorneInclu(descriptif.getBaremeGeneral().getBaremeTat().getOnBorneInclu());
                valeurATranche.setOnBorneMini(descriptif.getBaremeGeneral().getBaremeTat().getOnBorneMini());
                valeurATranche.setOnCumulatif(descriptif.getBaremeGeneral().getBaremeTat().getOnCumulatif());
            } else {
                ValeursBaremeSansTrancheValeur valeursTrouvees = restituerValeursBaremeSansTranche(pCriteres);
                
                if (valeursTrouvees != null) {
                    retourMethode.setTheValeursBaremeSansTrancheValeur(valeursTrouvees);
                    retourMethode.setCdBareme(pCriteres.getCdBareme());
                    retourMethode.setTypeUtilite(descriptif.getTypeUtilite());
                    retourMethode.setDtFinVldtBareme(descriptif.getDtFinVldtBareme());
                }
            }
        }
        
        return retourMethode;
    }
    
    public void controlerParametresAppel(RechercheValeursBaremeValeur pCriteres) throws ListeAnomaliesErreur {
        ArrayList<AnomalieErreur> listeAnomalies = new ArrayList<AnomalieErreur>();
        
        if (pCriteres.getDateLeg1() != null) {
            coherenceMetier.verifierDateDansCalendrier(pCriteres.getDateLeg1(), listeAnomalies);
        }
        
        if (pCriteres.getDateLeg2() != null) {
            coherenceMetier.verifierDateDansCalendrier(pCriteres.getDateLeg2(), listeAnomalies);
        }
        
        if (pCriteres.getDateLeg1() != null && pCriteres.getDateLeg2() != null && listeAnomalies.size() == 0 && pCriteres.getDateLeg2().before(pCriteres.getDateLeg1())) {
            listeAnomalies.add(constructionAnomalieMetier.construireAnomalie(AnomalieValeur.CODE_ANO_F006));
        }
        
        if (pCriteres.getMontant() != null && pCriteres.getMontant().doubleValue() < 0) {
            listeAnomalies.add(constructionAnomalieMetier.construireAnomalie(AnomalieValeur.CODE_ANO_F008));
        }
        
        if (listeAnomalies.size() > 0) {
            AnomalieErreur[] anomalies = new AnomalieErreur[listeAnomalies.size()];
            anomalies = (AnomalieErreur[]) listeAnomalies.toArray(anomalies);
            
            ListeAnomaliesErreur anomaliesALever = new ListeAnomaliesErreur();
            anomaliesALever.setListeAnomalieErreur(anomalies);
            
            throw anomaliesALever;
        }
    }
    
    public ValeursBaremeSansTrancheValeur restituerValeursBaremeSansTranche(RechercheValeursBaremeValeur pCriteres) {
        ValeursBaremeSansTrancheValeur resultatRecherche = daoValeurs.restituerValeursBaremeSansTranche(pCriteres.getCdBareme());
        
        if ((resultatRecherche.getTheValeurBaremeValeur() != null) && (resultatRecherche.getTheValeurBaremeValeur().length > 0)) {
            if ((pCriteres.getDateLeg1() != null) || (pCriteres.getDateLeg2() != null)) {
                ValeurBaremeValeur[] valeursFiltrees = this.filtrerValeursDateDebutVldt(resultatRecherche.getTheValeurBaremeValeur(), pCriteres.getDateLeg1(), pCriteres.getDateLeg2());
                resultatRecherche.setTheValeurBaremeValeur(valeursFiltrees);
                resultatRecherche.setNbValBareme(valeursFiltrees.length);
            }
        } else {
            resultatRecherche.setNbValBareme(0);
        }
        
        return resultatRecherche;
    }
    
    public ValeurBaremeValeur[] filtrerValeursDateDebutVldt(ValeurBaremeValeur[] pListeValeurs, Calendar pDateLeg1, Calendar pDateLeg2) {
        
        List<ValeurBaremeValeur> listeRetour = new ArrayList<ValeurBaremeValeur>();
        ValeurBaremeValeur[] retour;
        
        Calendar derniereDateInferieurDateDebut = null;
        
        ValeurBaremeValeur valeurBaremeDernierPrecedentDebut = null;
        
        ValeurBaremeValeur valBareme = new ValeurBaremeValeur();
        
        for (Iterator<ValeurBaremeValeur> it = Arrays.asList(pListeValeurs).iterator(); it.hasNext();) {
            valBareme = (ValeurBaremeValeur) it.next();
            
            if (pDateLeg1 == null || valBareme.getDebVldtVlBareme().after(pDateLeg1)) {
                if ((pDateLeg2 != null) && !valBareme.getDebVldtVlBareme().after(pDateLeg2)) {
                    
                    listeRetour.add(valBareme);
                }
                
            } else {
                
                if ((derniereDateInferieurDateDebut == null) || valBareme.getDebVldtVlBareme().after(derniereDateInferieurDateDebut)) {
                    
                    derniereDateInferieurDateDebut = valBareme.getDebVldtVlBareme();
                    valeurBaremeDernierPrecedentDebut = valBareme;
                }
                
            }
            
        }
        
        if (valeurBaremeDernierPrecedentDebut != null) {
            listeRetour.add(valeurBaremeDernierPrecedentDebut);
        }
        
        retour = new ValeurBaremeValeur[listeRetour.size()];
        
        retour = (ValeurBaremeValeur[]) listeRetour.toArray(retour);
        
        return retour;
    }
}
