/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.domaine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import fr.gouv.impots.appli.capromas.transverse.udd.metier.ElementFiscalCalculeValeur;
import fr.gouv.impots.appli.capromas.transverse.udd.metier.ElementFiscalValeur;
import fr.gouv.impots.appli.capromas.domaine.TableElementFiscal;

public class CalculElementFiscalMetier {
    
    public CalculElementFiscalMetier() {
        super();
    }
    
    public final ElementFiscalCalculeValeur[] calculerDelta(ElementFiscalCalculeValeur[] listeEfCalcules, ElementFiscalValeur[] listeEF) {
        ElementFiscalValeur elemFiscEntree;
        ElementFiscalCalculeValeur eltFiscalCalc;
        int compteur = 0;
        
        ElementFiscalCalculeValeur[] retour = new ElementFiscalCalculeValeur[listeEfCalcules.length];
        
        TableElementFiscal tableEltFiscaux = new TableElementFiscal(listeEF);
        
        for (Iterator<ElementFiscalCalculeValeur> it = Arrays.asList(listeEfCalcules).iterator(); it.hasNext();) {
            eltFiscalCalc = (ElementFiscalCalculeValeur) it.next();
            
            elemFiscEntree = tableEltFiscaux.getElementFiscal(eltFiscalCalc.getCdEFisc(), eltFiscalCalc.getNoDistinctionEf());
            
            if (elemFiscEntree == null) {
                eltFiscalCalc.setVlDelta(null);
            } else {
                Double delta = UtilitaireMetier.soustraire(eltFiscalCalc, elemFiscEntree);
                eltFiscalCalc.setVlDelta(delta);
                
            }
            
            retour[compteur] = eltFiscalCalc;
            compteur++;
        }
        
        return retour;
    }
    
    public final ElementFiscalCalculeValeur[] purgerValeursNulles(ElementFiscalCalculeValeur[] listeEfCalculs) {
        ElementFiscalCalculeValeur[] elementsPurges = new ElementFiscalCalculeValeur[0];
        
        if ((listeEfCalculs != null) && (listeEfCalculs.length > 0)) {
            List<ElementFiscalCalculeValeur> elementsValorises = new ArrayList<ElementFiscalCalculeValeur>();
            
            for (int i = 0; i < listeEfCalculs.length; i++) {
                if ((listeEfCalculs[i] != null) && listeEfCalculs[i].estValorise()) {
                    elementsValorises.add(listeEfCalculs[i]);
                }
            }
            
            if (elementsValorises.size() > 0) {
                elementsPurges = (ElementFiscalCalculeValeur[]) elementsValorises.toArray(elementsPurges);
            }
        }
        
        return elementsPurges;
    }

    public final ElementFiscalValeur[] valoriserEltZero(String[] listeCdEF, ElementFiscalValeur[] listeEF) {
        TableElementFiscal tableEltFiscaux = new TableElementFiscal(listeEF);
        return this.valoriserEltZero(listeCdEF, tableEltFiscaux);
    }
    
    private ElementFiscalValeur[] valoriserEltZero(String[] listeCdEF, TableElementFiscal tableEltFiscaux) {
        
        ElementFiscalValeur[] retour = new ElementFiscalValeur[0];
        
        List<ElementFiscalValeur> listeRetour = new ArrayList<ElementFiscalValeur>();
        
        if ((listeCdEF != null) && (listeCdEF.length > 0) && (tableEltFiscaux != null)) {
            
            ElementFiscalValeur elemFiscal = new ElementFiscalValeur();
            
            for (Iterator<String> it = Arrays.asList(listeCdEF).iterator(); it.hasNext();) {
                String codeElementFiscal = (String) it.next();
                
                if (codeElementFiscal != null) {
                    
                    elemFiscal = tableEltFiscaux.getElementFiscal(codeElementFiscal, ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI);
                    
                    if (elemFiscal == null) {
                        elemFiscal = new ElementFiscalValeur(codeElementFiscal, Double.valueOf(0.0));
                    } else if (!elemFiscal.estValorise()) {
                        elemFiscal.setVlEFisc(new Double(0.0));
                    }
                    
                    listeRetour.add(elemFiscal);
                }
            }
            
            retour = (ElementFiscalValeur[]) listeRetour.toArray(new ElementFiscalValeur[listeRetour.size()]);
        }
        
        return retour;
    }
    
}