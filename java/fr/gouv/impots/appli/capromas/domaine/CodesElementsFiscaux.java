/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.domaine;

public interface CodesElementsFiscaux {
    
    String EF_500021 = "500021";
    String EF_500022 = "500022";
    String EF_500023 = "500023";
    String EF_500024 = "500024";
    String EF_500025 = "500025";
    String EF_500026 = "500026";
    String EF_500027 = "500027";
    String EF_500028 = "500028";
    String EF_500029 = "500029";
    String EF_500030 = "500030";
    String EF_500031 = "500031";
    String EF_500032 = "500032";
    String EF_500033 = "500033";
    String EF_500034 = "500034";
    String EF_500036 = "500036";
    String EF_500038 = "500038";
    String EF_500041 = "500041";
    String EF_500042 = "500042";
    String EF_500043 = "500043";
    String EF_500047 = "500047";
    String EF_500048 = "500048";
    String EF_500049 = "500049";
    String EF_500052 = "500052";
    String EF_500053 = "500053";
    String EF_500054 = "500054";
    
    String EF_900763 = "900763";
    String EF_900764 = "900764";
    String EF_900767 = "900767";
    String EF_900769 = "900769";
    String EF_900771 = "900771";
    String EF_900772 = "900772";
    
    String EF_901364 = "901364";
    
    String EF_902418 = "902418";
    String EF_902419 = "902419";
    String EF_902447 = "902447";
    
    String EF_904110 = "904110";
    String EF_904111 = "904111";
    String EF_904112 = "904112";
    
    String EF_906443 = "906443";
    String EF_906444 = "906444";
    String EF_906449 = "906449";
    String EF_906450 = "906450";
    String EF_906451 = "906451";
    String EF_906452 = "906452";
    String EF_906453 = "906453";
    String EF_906454 = "906454";
    String EF_906455 = "906455";
    String EF_906456 = "906456";
    String EF_906457 = "906457";
    String EF_906459 = "906459";
    String EF_906460 = "906460";
    String EF_906461 = "906461";
    String EF_906464 = "906464";
    String EF_906465 = "906465";
    String EF_906466 = "906466";
    String EF_906467 = "906467";
    String EF_906468 = "906468";
    String EF_906469 = "906469";
    String EF_906470 = "906470";
    String EF_906471 = "906471";
    String EF_906472 = "906472";
    String EF_906473 = "906473";
    String EF_906475 = "906475";
    String EF_906481 = "906481";
    String EF_906482 = "906482";
    
    String EF_906504 = "906504";
    String EF_906506 = "906506";
    String EF_906507 = "906507";
    String EF_906508 = "906508";
    
    String EF_906564 = "906564";
    String EF_906565 = "906565";
    String EF_906566 = "906566";
    String EF_906567 = "906567";
    String EF_906570 = "906570";
    String EF_906571 = "906571";
    String EF_906572 = "906572";
    String EF_906573 = "906573";
    String EF_906576 = "906576";
    String EF_906577 = "906577";
    
    String EF_907017 = "907017";
    String EF_907018 = "907018";
    String EF_908549 = "908549";
    String EF_908550 = "908550";
    String EF_909419 = "909419";
    String EF_909420 = "909420";
    
}