/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse;

public final class ValeursBaremeSansTrancheValeur {
    
    private int nbValBareme = 0;
    
    private ValeurBaremeValeur[] theValeurBaremeValeur;
    
    public ValeursBaremeSansTrancheValeur() {
        super();
    }
    
    public void setNbValBareme(int nbValBareme) {
        this.nbValBareme = nbValBareme;
    }
    
    public int getNbValBareme() {
        return this.nbValBareme;
    }
    
    public void setTheValeurBaremeValeur(ValeurBaremeValeur[] newTheValeurBaremeValeur) {
        this.theValeurBaremeValeur = newTheValeurBaremeValeur;
    }
    
    public ValeurBaremeValeur[] getTheValeurBaremeValeur() {
        return this.theValeurBaremeValeur;
    }
    
    public void setTheValeurBaremeValeur(int index, ValeurBaremeValeur newTheValeurBaremeValeur) {
        this.theValeurBaremeValeur[index] = newTheValeurBaremeValeur;
    }
    
    public ValeurBaremeValeur getTheValeurBaremeValeur(int index) {
        return this.theValeurBaremeValeur[index];
    }
    
}
