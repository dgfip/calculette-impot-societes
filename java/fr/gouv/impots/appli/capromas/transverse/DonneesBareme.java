/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse;

public final class DonneesBareme {
    
    public static final int TYPE_VALEUR_QUANTITE = 1;
    
    public static final int TYPE_VALEUR_TAUX = 2;
    
    public static final String TYPE_UTILITE_LIMITE = "LIM";
    
    public static final String TYPE_UTILITE_TST = "TST";
    
    public static final String TYPE_UTILITE_TAT = "TAT";
    
    public static final String TYPE_UTILITE_ATT = "ATT";
    
    public static final String TYPE_UTILITE_CLE = "CLE";
    
    private DonneesBareme() {
        super();
    }
    
}
