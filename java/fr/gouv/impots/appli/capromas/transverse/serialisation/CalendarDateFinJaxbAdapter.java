/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse.serialisation;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import fr.gouv.impots.appli.commun.frameworks.exceptions.ConfigurationException;
import fr.gouv.impots.appli.commun.frameworks.util.DateUtils;

public final class CalendarDateFinJaxbAdapter extends XmlAdapter<String, Calendar> {
    
    public static final String FORMAT_DATE = "yyyy-MM-dd";
    
    private static final int MS_999 = 999;
    
    private static final int MINUTE_59 = 59;
    
    private static final int SECONDE_59 = 59;
    
    private static final int HEURE_23 = 23;
    
    public CalendarDateFinJaxbAdapter() {
        super();
    }
    
    public String marshal(Calendar calendar) throws Exception {
        String retour = null;
        if (calendar != null) {
            Calendar calendarCasteHeureParis = (Calendar) calendar;
            Calendar calendarGMT = DateUtils.changerTimeZoneParis(calendarCasteHeureParis);
            
            SimpleDateFormat formatteur = new SimpleDateFormat(FORMAT_DATE, Locale.FRANCE);
            retour = formatteur.format(calendarGMT.getTime());
        }
        return retour;
    }
    
    public Calendar unmarshal(String chaine) throws Exception {
        Calendar retour = null;
        if (chaine != null) {
            String chaineCaste = (String) chaine;
            try {
                Date date = (DateUtils.creerSimpleDateFormatParis(FORMAT_DATE)).parse(chaineCaste);
                retour = new GregorianCalendar();
                retour.setTime(date);
                retour.add(Calendar.HOUR_OF_DAY, HEURE_23);
                retour.add(Calendar.MINUTE, MINUTE_59);
                retour.add(Calendar.SECOND, SECONDE_59);
                retour.add(Calendar.MILLISECOND, MS_999);
                retour = DateUtils.changerTimeZone(retour, DateUtils.TIME_ZONE_GMT);
            } catch (ParseException e) {
                throw new ConfigurationException("Problème de convertion en date de la chaine : " + chaine, e);
            }
        }
        return retour;
    }
    
    public Class<GregorianCalendar> getFieldType() {
        return GregorianCalendar.class;
    }
}