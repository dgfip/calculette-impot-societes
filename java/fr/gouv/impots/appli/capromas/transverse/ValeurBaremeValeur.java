/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse;

import java.util.Calendar;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

public class ValeurBaremeValeur {
    
    private String cdBareme;
    
    private Calendar debVldtVlBareme;
    
    private Double vlNumBaremeTaux;
    
    private Integer vlDenBaremeTaux;
    
    public ValeurBaremeValeur() {
        super();
    }
    
    @XmlElement(name = "DebVldtVlBareme")
    @XmlJavaTypeAdapter(fr.gouv.impots.appli.capromas.transverse.serialisation.CalendarDateDebutJaxbAdapter.class)
    public final Calendar getDebVldtVlBareme() {
        return debVldtVlBareme;
    }
    
    public final void setDebVldtVlBareme(Calendar newDateDebVldtVlBareme) {
        this.debVldtVlBareme = newDateDebVldtVlBareme;
    }
    
    @XmlElement(name = "VlBaremeDenTaux")
    public final Integer getVlDenBaremeTaux() {
        return vlDenBaremeTaux;
    }
    
    public final void setVlDenBaremeTaux(Integer newIntegerVlDenBaremeTaux) {
        this.vlDenBaremeTaux = newIntegerVlDenBaremeTaux;
    }
    
    @XmlElement(name = "VlBaremeNumTaux")
    public final Double getVlNumBaremeTaux() {
        return vlNumBaremeTaux;
    }
    
    public final void setVlNumBaremeTaux(Double newDblVlNumBaremeTaux) {
        this.vlNumBaremeTaux = newDblVlNumBaremeTaux;
    }
    
    @XmlElement(name = "CdBareme")
    public final String getCdBareme() {
        return cdBareme;
    }
    
    public final void setCdBareme(String newCdBareme) {
        this.cdBareme = newCdBareme;
    }
    
    public final Double getValeurBaremeTaux() {
        return vlNumBaremeTaux / vlDenBaremeTaux;
    }
}
