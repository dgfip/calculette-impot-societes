/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse.anomalies;

import fr.gouv.impots.appli.capromas.transverse.udd.anomalies.AnomalieF210Valeur;

public final class AnomalieF210Erreur extends AnomalieErreur {
    
    private static final long serialVersionUID = -5444044126022939770L;
    
    public AnomalieF210Erreur(final AnomalieF210Valeur anomalieF210Valeur) {
        super(anomalieF210Valeur);
    }
    
    public void setAnomalieF210Valeur(AnomalieF210Valeur anomalieF210Valeur) {
        super.setAnomalieValeur(anomalieF210Valeur);
    }
    
    public AnomalieF210Valeur getAnomalieF210Valeur() {
        return (AnomalieF210Valeur) super.getAnomalieValeur();
    }
}
