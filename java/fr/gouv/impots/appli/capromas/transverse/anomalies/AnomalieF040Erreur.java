/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse.anomalies;

import fr.gouv.impots.appli.capromas.transverse.udd.anomalies.AnomalieF040Valeur;

public final class AnomalieF040Erreur extends AnomalieErreur {
    
    private static final long serialVersionUID = -1392092470754150306L;
    
    public AnomalieF040Erreur(final AnomalieF040Valeur anomalieF040Valeur) {
        super(anomalieF040Valeur);
    }
    
    public void setAnomalieF040Valeur(AnomalieF040Valeur anomalieF040Valeur) {
        super.setAnomalieValeur(anomalieF040Valeur);
    }
    
    public AnomalieF040Valeur getAnomalieF040Valeur() {
        return (AnomalieF040Valeur) super.getAnomalieValeur();
    }
}
