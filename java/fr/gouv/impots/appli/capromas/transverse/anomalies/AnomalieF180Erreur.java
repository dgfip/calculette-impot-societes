/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse.anomalies;

import fr.gouv.impots.appli.capromas.transverse.udd.anomalies.AnomalieF180Valeur;

public final class AnomalieF180Erreur extends AnomalieErreur {
    
    private static final long serialVersionUID = 2804050042373854885L;
    
    public AnomalieF180Erreur(final AnomalieF180Valeur anomalieF180Valeur) {
        super(anomalieF180Valeur);
    }
    
    public void setAnomalieF180Valeur(AnomalieF180Valeur anomalieF180Valeur) {
        super.setAnomalieValeur(anomalieF180Valeur);
    }
    
    public AnomalieF180Valeur getAnomalieF180Valeur() {
        return (AnomalieF180Valeur) super.getAnomalieValeur();
    }
}
