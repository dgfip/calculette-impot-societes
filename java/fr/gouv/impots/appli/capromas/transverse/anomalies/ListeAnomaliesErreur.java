/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse.anomalies;

public final class ListeAnomaliesErreur extends Exception {
    
    private static final long serialVersionUID = -7331798154277019204L;
    
    private AnomalieErreur[] listeAnomalieErreur;
    
    public ListeAnomaliesErreur() {
        super();
    }
    
    public void setListeAnomalieErreur(AnomalieErreur[] listeAnomalieErreur) {
        this.listeAnomalieErreur = listeAnomalieErreur;
    }
    
    public AnomalieErreur[] getListeAnomalieErreur() {
        return this.listeAnomalieErreur;
    }
    
    public void setListeAnomalieErreur(int index, AnomalieErreur newListeAnomalieErreur) {
        this.listeAnomalieErreur[index] = newListeAnomalieErreur;
    }
    
    public AnomalieErreur getListeAnomalieErreur(int index) {
        return this.listeAnomalieErreur[index];
    }
}
