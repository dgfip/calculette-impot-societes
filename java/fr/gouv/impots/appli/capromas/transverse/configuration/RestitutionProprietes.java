/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse.configuration;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import fr.gouv.impots.appli.commun.frameworks.exceptions.ConfigurationException;
import fr.gouv.impots.appli.commun.frameworks.util.locator.ChargeurProprietes;

public final class RestitutionProprietes {
    
    private static final String CODE_APPLI_VERSION = "application.version";
    
    private static ChargeurProprietes instanceChargeurProprietes;
    
    private RestitutionProprietes() {
        super();
    }
    
    public static ChargeurProprietes getChargeurProprietes() {
        if (null == instanceChargeurProprietes) {
            
            try {
                @SuppressWarnings("unused")
                Context contexte = new InitialContext();
                
                instanceChargeurProprietes = new ChargeurProprietes(ConfigurationInterne.getProprietes(CODE_APPLI_VERSION));
            } catch (NamingException e) {
                throw new ConfigurationException("Erreur durant l'initialisation de l'application", e);
            }
        }
        
        return instanceChargeurProprietes;
    }
}
