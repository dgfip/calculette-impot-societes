/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse.configuration;

public final class ConfigurationInterne {
    
    private static final String CHEMIN_CONF = "/capromas.properties";
    
    private static LecteurProprietes lecteurPropriete = null;
    
    static {
        lecteurPropriete = new LecteurProprietes(CHEMIN_CONF, true);
    }
    
    private ConfigurationInterne() {
        super();
    }
    
    public static String getProprietes(String cle) {
        return lecteurPropriete.getPropriete(cle);
    }
}
