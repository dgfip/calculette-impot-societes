/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse.configuration;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import fr.gouv.impots.appli.commun.frameworks.exceptions.ConfigurationException;

public final class LecteurProprietes {
    
    private transient Properties proprietes = null;
    
    public LecteurProprietes(final String cheminFichierProprietes, final boolean isClasspath) {
        chargerProprietes(cheminFichierProprietes, isClasspath);
    }
    
    private void chargerProprietes(final String cheminFichierProprietes, final boolean isClasspath) {
        InputStream flux = null;
        
        if (isClasspath) {
            flux = getClass().getResourceAsStream(cheminFichierProprietes);
        } else {
            try {
                flux = new FileInputStream(cheminFichierProprietes);
            } catch (FileNotFoundException e) {
                throw new ConfigurationException("Erreur au chargement d'un fichier de propriétés. " + "Impossible d'acceder au fichier .properties spécifié : " + cheminFichierProprietes, e);
            }
        }
        
        if (flux == null) {
            throw new ConfigurationException("Erreur au chargement des propriétés");
        }
        try {
            proprietes = new Properties();
            proprietes.load(flux);
        } catch (IOException e) {
            throw new ConfigurationException("Erreur au chargement des propriétés", e);
        }
    }
    
    public String getPropriete(final String cle) {
        return proprietes.getProperty(cle);
    }
    
}
