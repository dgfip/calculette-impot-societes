/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse.erreurs;

import fr.gouv.impots.appli.capromas.transverse.udd.erreurs.F007Erreur;
import fr.gouv.impots.appli.capromas.transverse.udd.erreurs.F015Erreur;
import fr.gouv.impots.appli.capromas.transverse.udd.erreurs.F020Erreur;
import fr.gouv.impots.appli.capromas.transverse.udd.erreurs.F030Erreur;
import fr.gouv.impots.appli.capromas.transverse.udd.erreurs.F031Erreur;
import fr.gouv.impots.appli.capromas.transverse.udd.erreurs.F090Erreur;
import fr.gouv.impots.appli.capromas.transverse.udd.erreurs.F100Erreur;
import fr.gouv.impots.appli.commun.frameworks.gestionerreurs.erreurs.FonctionnelleErreur;
import fr.gouv.impots.appli.commun.frameworks.gestionerreurs.exceptions.ErreurInterface;
import fr.gouv.impots.appli.commun.frameworks.gestionerreurs.transverse.GestionnaireErreurs;

public final class CaproErreurRetourFactory {
    
    public CaproErreurRetourFactory() {
        super();
    }
    
    public F007Erreur initialiserF007Erreur() {
        ErreurInterface erreurInterf = GestionnaireErreurs.construireErreur(CaproCodeErreur.CODE_DATE_FISC_INCONNUE);
        
        F007Erreur erreur = new F007Erreur();
        remplirChampsErreur(erreurInterf, erreur);
        
        return erreur;
    }
    
    public F015Erreur initialiserF015Erreur() {
        ErreurInterface erreurInterf = GestionnaireErreurs.construireErreur(CaproCodeErreur.CODE_REF_EF_ABSENT_DE_NREF);
        
        F015Erreur erreur = new F015Erreur();
        remplirChampsErreur(erreurInterf, erreur);
        
        return erreur;
    }
    
    public F020Erreur initialiserF020Erreur() {
        ErreurInterface erreurInterf = GestionnaireErreurs.construireErreur(CaproCodeErreur.CODE_EF_SANS_NUMERO_DISTINCTION);
        
        F020Erreur erreur = new F020Erreur();
        remplirChampsErreur(erreurInterf, erreur);
        
        return erreur;
    }
    
    public F030Erreur initialiserF030Erreur() {
        ErreurInterface erreurInterf = GestionnaireErreurs.construireErreur(CaproCodeErreur.CODE_NUMERO_DISTINCTION_REPETE);
        
        F030Erreur erreur = new F030Erreur();
        remplirChampsErreur(erreurInterf, erreur);
        
        return erreur;
    }
    
    public F031Erreur initialiserF031Erreur() {
        ErreurInterface erreurInterf = GestionnaireErreurs.construireErreur(CaproCodeErreur.CODE_REF_EF_REPETE);
        
        F031Erreur erreur = new F031Erreur();
        remplirChampsErreur(erreurInterf, erreur);
        
        return erreur;
    }
    
    public F090Erreur initialiserF090Erreur() {
        ErreurInterface erreurInterf = GestionnaireErreurs.construireErreur(CaproCodeErreur.CODE_EF_NON_RENSEIGNEE);
        
        F090Erreur erreur = new F090Erreur();
        remplirChampsErreur(erreurInterf, erreur);
        
        return erreur;
    }
    
    public F100Erreur initialiserF100Erreur() {
        ErreurInterface erreurInterf = GestionnaireErreurs.construireErreur(CaproCodeErreur.CODE_EF_NON_VALORISE);
        
        F100Erreur erreur = new F100Erreur();
        remplirChampsErreur(erreurInterf, erreur);
        
        return erreur;
    }
    
    public FonctionnelleErreur initialiserErreurFonctionnelle(String codeErreur) {
        ErreurInterface erreurInterf = null;
        int codeErreurInt = -1;
        
        if ((codeErreur != null) && (codeErreur.length() > 0)) {
            try {
                codeErreurInt = Integer.parseInt(codeErreur.substring(1));
            } catch (NumberFormatException e) {
            }
        }
        
        switch (codeErreurInt) {
            case CaproCodeErreur.CODE_ERR_F002:
                erreurInterf = GestionnaireErreurs.construireErreur(CaproCodeErreur.DOCUMENTS_EN_ENTREE_INEXISTANTS);
                break;
            
            case CaproCodeErreur.CODE_ERR_F003:
                erreurInterf = GestionnaireErreurs.construireErreur(CaproCodeErreur.DOCUMENTS_EN_ENTREE_INCORRECTS);
                break;
            
            case CaproCodeErreur.CODE_ERR_F004:
                erreurInterf = GestionnaireErreurs.construireErreur(CaproCodeErreur.CODE_DATE_ET_PERIODE_CONCURRENTES);
                break;
            
            case CaproCodeErreur.CODE_ERR_F005:
                erreurInterf = GestionnaireErreurs.construireErreur(CaproCodeErreur.CODE_ABSENCE_PER_FISC);
                break;
            
            case CaproCodeErreur.CODE_ERR_F006:
                erreurInterf = GestionnaireErreurs.construireErreur(CaproCodeErreur.CODE_PER_FISC_INCOHERENTE);
                break;
            
            case CaproCodeErreur.CODE_ERR_F008:
                erreurInterf = GestionnaireErreurs.construireErreur(CaproCodeErreur.CODE_MONTANT_NEGATIF);
                break;
            
            case CaproCodeErreur.CODE_ERR_F090:
                erreurInterf = GestionnaireErreurs.construireErreur(CaproCodeErreur.CODE_ABSENCE_EF_A_PENALISER);
                break;
            
            default:
                break;
        }
        
        FonctionnelleErreur erreur = new FonctionnelleErreur();
        remplirChampsErreur(erreurInterf, erreur);
        
        return erreur;
    }
    
    private void remplirChampsErreur(ErreurInterface foncEx, FonctionnelleErreur erreur) {
        if (foncEx == null || erreur == null) {
            if (erreur != null) {
                erreur.setCode(-1);
                erreur.setSeverite(-1);
            }
        } else {
            erreur.setCode(foncEx.getCode());
            erreur.setDescriptif(foncEx.getDescriptif());
            erreur.setId(foncEx.getId());
            erreur.setLibelle(foncEx.getLibelle());
            erreur.setSeverite(foncEx.getSeverite());
        }
    }
}
