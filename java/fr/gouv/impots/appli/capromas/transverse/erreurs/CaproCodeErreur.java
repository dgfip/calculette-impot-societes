/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse.erreurs;

import fr.gouv.impots.appli.commun.frameworks.gestionerreurs.transverse.AbstractCodeErreur;

public final class CaproCodeErreur extends AbstractCodeErreur {
    
    public final static int CODE_ERR_F002 = 2;
    
    public final static int CODE_ERR_F003 = 3;
    
    public final static int CODE_ERR_F004 = 4;
    
    public final static int CODE_ERR_F005 = 5;
    
    public final static int CODE_ERR_F006 = 6;
    
    public final static int CODE_ERR_F007 = 7;
    
    public final static int CODE_ERR_F008 = 8;
    
    public final static int CODE_ERR_F010 = 10;
    
    public final static int CODE_ERR_F015 = 15;
    
    public final static int CODE_ERR_F020 = 20;
    
    public final static int CODE_ERR_F021 = 21;
    
    public final static int CODE_ERR_F030 = 30;
    
    public final static int CODE_ERR_F031 = 31;
    
    public final static int CODE_ERR_F040 = 40;
    
    public final static int CODE_ERR_F045 = 45;
    
    public final static int CODE_ERR_F050 = 50;
    
    public final static int CODE_ERR_F051 = 51;
    
    public final static int CODE_ERR_F060 = 60;
    
    public final static int CODE_ERR_F070 = 70;
    
    public final static int CODE_ERR_F080 = 80;
    
    public final static int CODE_ERR_F090 = 90;
    
    public final static int CODE_ERR_F100 = 100;
    
    public final static int CODE_ERR_F110 = 110;
    
    public final static int CODE_ERR_F120 = 120;
    
    public final static int CODE_ERR_F130 = 130;
    
    public final static int CODE_ERR_F140 = 140;
    
    public final static int CODE_ERR_F141 = 141;
    
    public final static int CODE_ERR_F150 = 150;
    
    public final static int CODE_ERR_F160 = 160;
    
    public final static int CODE_ERR_F170 = 170;
    
    public final static int CODE_ERR_F171 = 171;
    
    public final static int CODE_ERR_F180 = 180;
    
    public final static int CODE_ERR_F190 = 190;
    
    public final static int CODE_ERR_F195 = 195;
    
    public final static int CODE_ERR_F200 = 200;
    
    public final static int CODE_ERR_F205 = 205;
    
    public final static int CODE_ERR_F210 = 210;
    
    public final static int CODE_ERR_F220 = 220;
    
    public final static int CODE_ERR_F225 = 225;

    public static final CaproCodeErreur DOCUMENTS_EN_ENTREE_INCORRECTS = new CaproCodeErreur(CODE_ERR_F003, TYPE_FONCTIONNELLE);
    
    public static final CaproCodeErreur DOCUMENTS_EN_ENTREE_INEXISTANTS = new CaproCodeErreur(CODE_ERR_F002, TYPE_FONCTIONNELLE);
    
    public static final CaproCodeErreur CODE_ABSENCE_PER_FISC = new CaproCodeErreur(CODE_ERR_F005, TYPE_FONCTIONNELLE);
    
    public static final CaproCodeErreur CODE_PER_FISC_INCOHERENTE = new CaproCodeErreur(CODE_ERR_F006, TYPE_FONCTIONNELLE);
    
    public static final CaproCodeErreur CODE_DATE_FISC_INCONNUE = new CaproCodeErreur(CODE_ERR_F007, TYPE_FONCTIONNELLE);
    
    public static final CaproCodeErreur CODE_EF_SANS_NUMERO_DISTINCTION = new CaproCodeErreur(CODE_ERR_F020, TYPE_FONCTIONNELLE);
    
    public static final CaproCodeErreur CODE_OPTION_RETOUR_CALCULS_INTERMEDIAIRES_INCONNU = new CaproCodeErreur(CODE_ERR_F021, TYPE_FONCTIONNELLE);
    
    public static final CaproCodeErreur CODE_REF_EF_REPETE = new CaproCodeErreur(CODE_ERR_F031, TYPE_FONCTIONNELLE);
    
    public static final CaproCodeErreur CODE_NUMERO_DISTINCTION_REPETE = new CaproCodeErreur(CODE_ERR_F030, TYPE_FONCTIONNELLE);
    
    public static final CaproCodeErreur CODE_DATE_MISEDEMEURE_INF_DATEDEPOT_DATELIMITEDEPOT = new CaproCodeErreur(CODE_ERR_F060, TYPE_FONCTIONNELLE);
    
    public static final CaproCodeErreur CODE_PAS_MANQUEMENT_SANCTIONNABLE = new CaproCodeErreur(CODE_ERR_F070, TYPE_FONCTIONNELLE);
    
    public static final CaproCodeErreur CODE_MODELEDOC_NON_DECLARATION_ASSIETTE = new CaproCodeErreur(CODE_ERR_F080, TYPE_FONCTIONNELLE);
    
    public static final CaproCodeErreur CODE_ABSENCE_EF_A_PENALISER = new CaproCodeErreur(CODE_ERR_F090, TYPE_FONCTIONNELLE);
    
    public static final CaproCodeErreur CODE_EF_NON_VALORISE = new CaproCodeErreur(CODE_ERR_F100, TYPE_FONCTIONNELLE);
    
    public static final CaproCodeErreur CODE_EF_NON_RENSEIGNEE = new CaproCodeErreur(CODE_ERR_F090, TYPE_FONCTIONNELLE);
    
    public static final CaproCodeErreur CODE_DEPOT_INF_DATE_LIMITEDEPOT = new CaproCodeErreur(CODE_ERR_F130, TYPE_FONCTIONNELLE);
    
    public static final CaproCodeErreur CODE_DATE_ASSOUP_INF_DATE_LIMITE_DEPOT = new CaproCodeErreur(CODE_ERR_F140, TYPE_FONCTIONNELLE);
    
    public static final CaproCodeErreur CODE_DEPOT_INF_DATE_ASSOUP = new CaproCodeErreur(CODE_ERR_F141, TYPE_FONCTIONNELLE);
    
    public static final CaproCodeErreur CODE_ABSENCE_DATE_LIMITE_DEPOT = new CaproCodeErreur(CODE_ERR_F150, TYPE_FONCTIONNELLE);
    
    public static final CaproCodeErreur CODE_VALEUR_RANG_MISE_DEMEURE_INVALIDE = new CaproCodeErreur(CODE_ERR_F160, TYPE_FONCTIONNELLE);
    
    public static final CaproCodeErreur CODE_ABSENCE_RANG_MISE_DEMEURE = new CaproCodeErreur(CODE_ERR_F170, TYPE_FONCTIONNELLE);
    
    public static final CaproCodeErreur CODE_ABSENCE_DATE_MISE_DEMEURE = new CaproCodeErreur(CODE_ERR_F171, TYPE_FONCTIONNELLE);
    
    public static final CaproCodeErreur CODE_MONTANT_NEGATIF = new CaproCodeErreur(CODE_ERR_F008, TYPE_FONCTIONNELLE);
    
    public static final CaproCodeErreur CODE_AUCUN_BAREME_TROUVE = new CaproCodeErreur(CODE_ERR_F051, TYPE_FONCTIONNELLE);
    
    public static final CaproCodeErreur CODE_REF_EF_ABSENT_DE_NREF = new CaproCodeErreur(CODE_ERR_F015, TYPE_FONCTIONNELLE);
    
    public static final CaproCodeErreur CODE_DATE_ET_PERIODE_CONCURRENTES = new CaproCodeErreur(CODE_ERR_F004, TYPE_FONCTIONNELLE);
    
    private CaproCodeErreur(final int newCode, final String newType) {
        super();
        setCode(newCode);
        setType(newType);
    }
}
