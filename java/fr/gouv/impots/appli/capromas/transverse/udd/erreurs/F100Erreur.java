/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse.udd.erreurs;

import fr.gouv.impots.appli.capromas.transverse.udd.anomalies.AnomalieF100Valeur;

public final class F100Erreur extends ConteneurAnomalieErreur {
    
    private AnomalieF100Valeur[] anomaliesF100;
    
    public F100Erreur() {
        super();
    }
    
    public void setAnomaliesF100(AnomalieF100Valeur[] newAnomaliesF100Valeur) {
        this.anomaliesF100 = newAnomaliesF100Valeur;
    }
    
    public AnomalieF100Valeur[] getAnomaliesF100() {
        return this.anomaliesF100;
    }
    
    public void setAnomaliesF100(int index, AnomalieF100Valeur newAnomalieF100Valeur) {
        this.anomaliesF100[index] = newAnomalieF100Valeur;
    }
    
    public AnomalieF100Valeur getAnomaliesF100(int index) {
        return this.anomaliesF100[index];
    }
}
