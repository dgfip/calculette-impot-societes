/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse.udd.metier;

import java.util.Calendar;

public class PeriodeFiscaleValeur {
    
    private Calendar dtDebPerFisc;
    
    private Calendar dtFinPerFisc;
    
    private Calendar dtFisc;
    
    public PeriodeFiscaleValeur() {
        super();
    }
    
    public final void setDtDebPerFisc(Calendar newDtDebPerFisc) {
        this.dtDebPerFisc = newDtDebPerFisc;
    }
    
    public final Calendar getDtDebPerFisc() {
        return this.dtDebPerFisc;
    }
    
    public final void setDtFinPerFisc(Calendar newDtFinPerFisc) {
        this.dtFinPerFisc = newDtFinPerFisc;
    }
    
    public final Calendar getDtFinPerFisc() {
        return this.dtFinPerFisc;
    }
    
    public final void setDtFisc(Calendar newDtFisc) {
        this.dtFisc = newDtFisc;
    }
    
    public final Calendar getDtFisc() {
        return this.dtFisc;
    }
    
    public final boolean isDateFiscale() {
        return this.dtFisc != null;
    }
    
}
