/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse.udd.anomalies;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "anomalie")
public class AnomalieValeur {
    
    public final static String CODE_ANO_F002 = "F002";
    
    public final static String CODE_ANO_F003 = "F003";
    
    public final static String CODE_ANO_F004 = "F004";
    
    public final static String CODE_ANO_F005 = "F005";
    
    public final static String CODE_ANO_F006 = "F006";
    
    public final static String CODE_ANO_F007 = "F007";
    
    public final static String CODE_ANO_F008 = "F008";
    
    public final static String CODE_ANO_F010 = "F010";
    
    public final static String CODE_ANO_F015 = "F015";
    
    public final static String CODE_ANO_F020 = "F020";
    
    public final static String CODE_ANO_F021 = "F021";
    
    public final static String CODE_ANO_F030 = "F030";
    
    public final static String CODE_ANO_F031 = "F031";
    
    public final static String CODE_ANO_F040 = "F040";
    
    public final static String CODE_ANO_F045 = "F045";
    
    public final static String CODE_ANO_F050 = "F050";
    
    public final static String CODE_ANO_F051 = "F051";
    
    public final static String CODE_ANO_F060 = "F060";
    
    public final static String CODE_ANO_F070 = "F070";
    
    public final static String CODE_ANO_F080 = "F080";
    
    public final static String CODE_ANO_F090 = "F090";
    
    public final static String CODE_ANO_F100 = "F100";
    
    public final static String CODE_ANO_F110 = "F110";
    
    public final static String CODE_ANO_F120 = "F120";
    
    public final static String CODE_ANO_F130 = "F130";
    
    public final static String CODE_ANO_F140 = "F140";
    
    public final static String CODE_ANO_F141 = "F141";
    
    public final static String CODE_ANO_F150 = "F150";
    
    public final static String CODE_ANO_F160 = "F160";
    
    public final static String CODE_ANO_F170 = "F170";
    
    public final static String CODE_ANO_F171 = "F171";
    
    public final static String CODE_ANO_F180 = "F180";
    
    public final static String CODE_ANO_F190 = "F190";
    
    public final static String CODE_ANO_F195 = "F195";
    
    public final static String CODE_ANO_F200 = "F200";
    
    public final static String CODE_ANO_F205 = "F205";
    
    public final static String CODE_ANO_F210 = "F210";
    
    public final static String CODE_ANO_F220 = "F220";
    
    public final static String CODE_ANO_F225 = "F225";
    
    private String code;
    
    private String libelle;
    
    public AnomalieValeur() {
        super();
    }
    
    public AnomalieValeur(final String newCode) {
        this.code = newCode;
    }
    
    public final void setCode(String newCode) {
        this.code = newCode;
    }
    
    @XmlElement(name = "code")
    public final String getCode() {
        return this.code;
    }
    
    public final void setLibelle(String newLibelle) {
        this.libelle = newLibelle;
    }
    
    @XmlElement(name = "libelle")
    public final String getLibelle() {
        return this.libelle;
    }
    
}
