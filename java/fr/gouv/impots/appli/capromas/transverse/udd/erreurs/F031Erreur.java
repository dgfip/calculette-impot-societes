/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse.udd.erreurs;

import fr.gouv.impots.appli.capromas.transverse.udd.anomalies.AnomalieF031Valeur;

public final class F031Erreur extends ConteneurAnomalieErreur {
    
    private AnomalieF031Valeur[] anomaliesF031;
    
    public F031Erreur() {
        super();
    }
    
    public void setAnomaliesF031(AnomalieF031Valeur[] newAnomaliesF031) {
        this.anomaliesF031 = newAnomaliesF031;
    }
    
    public AnomalieF031Valeur[] getAnomaliesF031() {
        return this.anomaliesF031;
    }
    
    public void setAnomaliesF031(int index, AnomalieF031Valeur newAnomalieF031) {
        this.anomaliesF031[index] = newAnomalieF031;
    }
    
    public AnomalieF031Valeur getAnomaliesF031(int index) {
        return this.anomaliesF031[index];
    }
}
