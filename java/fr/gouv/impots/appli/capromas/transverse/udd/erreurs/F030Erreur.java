/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse.udd.erreurs;

import fr.gouv.impots.appli.capromas.transverse.udd.anomalies.AnomalieF030Valeur;

public final class F030Erreur extends ConteneurAnomalieErreur {
    
    private AnomalieF030Valeur[] anomaliesF030;
    
    public F030Erreur() {
        super();
    }
    
    public void setAnomaliesF030(AnomalieF030Valeur[] newAnomaliesF030) {
        this.anomaliesF030 = newAnomaliesF030;
    }
    
    public AnomalieF030Valeur[] getAnomaliesF030() {
        return this.anomaliesF030;
    }
    
    public void setAnomaliesF030(int index, AnomalieF030Valeur newAnomalieF030) {
        this.anomaliesF030[index] = newAnomalieF030;
    }
    
    public AnomalieF030Valeur getAnomaliesF030(int index) {
        return this.anomaliesF030[index];
    }
}
