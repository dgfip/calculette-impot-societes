/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse.udd.metier;

import javax.xml.bind.annotation.XmlTransient;

@XmlTransient
public class DocumentValeur {
    
    private String modeleDoc;
    
    private Integer noVersion;
    
    public DocumentValeur() {
        super();
    }
    
    public final void setModeleDoc(String newModeleDoc) {
        this.modeleDoc = newModeleDoc;
    }
    
    public final String getModeleDoc() {
        return this.modeleDoc;
    }
    
    public final void setNoVersion(Integer newNoVersion) {
        this.noVersion = newNoVersion;
    }
    
    public final Integer getNoVersion() {
        return this.noVersion;
    }
    
    public final boolean equals(Object obj) {
        boolean retour = false;
        
        if (this == obj) {
            retour = true;
        } else {
            if (obj == null) {
                retour = false;
            } else if ((modeleDoc == null) || (((DocumentValeur) obj).getModeleDoc() == null)) {
                retour = false;
            } else {
                retour = modeleDoc.equals(((DocumentValeur) obj).getModeleDoc());
            }
        }
        
        return retour;
    }
    
    public final int hashCode() {
        
        int cde = 0;
        
        if (modeleDoc != null) {
            cde = modeleDoc.hashCode();
        }
        
        return cde;
    }
    
}
