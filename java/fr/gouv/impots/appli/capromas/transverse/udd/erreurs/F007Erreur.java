/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse.udd.erreurs;

import fr.gouv.impots.appli.capromas.transverse.udd.anomalies.AnomalieF007Valeur;

public final class F007Erreur extends ConteneurAnomalieErreur {
    
    private AnomalieF007Valeur[] anomaliesF007;
    
    public F007Erreur() {
        super();
    }
    
    public void setAnomaliesF007(AnomalieF007Valeur[] anomaliesF007) {
        this.anomaliesF007 = anomaliesF007;
    }
    
    public AnomalieF007Valeur[] getAnomaliesF007() {
        return this.anomaliesF007;
    }
    
    public void setAnomaliesF007(int index, AnomalieF007Valeur newAnomaliesF007) {
        this.anomaliesF007[index] = newAnomaliesF007;
    }
    
    public AnomalieF007Valeur getAnomaliesF007(int index) {
        return this.anomaliesF007[index];
    }
}
