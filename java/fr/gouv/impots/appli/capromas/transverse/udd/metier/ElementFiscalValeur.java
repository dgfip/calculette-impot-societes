/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse.udd.metier;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ElementFiscalValeur", propOrder = { "cdEFisc", "noDistinctionEf", "vlEFisc" })
@XmlSeeAlso({ ElementFiscalCalculeValeur.class })
public class ElementFiscalValeur {
    
    public static final Integer NUMERO_DISTINCTION_NON_DEFINI = null;
    
    @XmlElement(required = true, nillable = true)
    protected String cdEFisc;
    @XmlElement(required = true, type = Integer.class, nillable = true)
    protected Integer noDistinctionEf;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double vlEFisc;
    
    public ElementFiscalValeur() {
        super();
    }
    
    public ElementFiscalValeur(final ElementFiscalValeur elementFiscalValeur) {
        super();
        
        if (elementFiscalValeur != null) {
            this.cdEFisc = elementFiscalValeur.getCdEFisc();
            this.noDistinctionEf = elementFiscalValeur.getNoDistinctionEf();
            this.vlEFisc = elementFiscalValeur.getVlEFisc();
        }
    }
    
    public ElementFiscalValeur(final String newCdEFisc, final Integer newNoDistinctionEf, final Double newVlEFisc) {
        super();
        this.cdEFisc = newCdEFisc;
        this.noDistinctionEf = newNoDistinctionEf;
        this.vlEFisc = newVlEFisc;
    }
    
    public ElementFiscalValeur(final String newCdEFisc, final Double newVlEFisc) {
        super();
        this.cdEFisc = newCdEFisc;
        this.noDistinctionEf = ElementFiscalValeur.NUMERO_DISTINCTION_NON_DEFINI;
        this.vlEFisc = newVlEFisc;
    }
    
    public String getCdEFisc() {
        return cdEFisc;
    }
    
    public void setCdEFisc(String value) {
        this.cdEFisc = value;
    }
    
    public Integer getNoDistinctionEf() {
        return noDistinctionEf;
    }
    
    public void setNoDistinctionEf(Integer value) {
        this.noDistinctionEf = value;
    }
    
    public Double getVlEFisc() {
        return vlEFisc;
    }
    
    public void setVlEFisc(Double value) {
        this.vlEFisc = value;
    }
    
    public final boolean estValorise() {
        return this.vlEFisc != null;
    }
    
    public final boolean equals(Object obj) {
        boolean retour = false;
        
        if (this == obj) {
            retour = true;
        } else {
            if (obj == null) {
                retour = false;
            } else if ((((ElementFiscalValeur) obj).getNoDistinctionEf()) == null) {
                retour = (this.getCdEFisc().equals(((ElementFiscalValeur) obj).getCdEFisc()));
            } else {
                retour = (this.getCdEFisc().equals(((ElementFiscalValeur) obj).getCdEFisc()) && ((ElementFiscalValeur) obj).getNoDistinctionEf().equals(this.getNoDistinctionEf()));
            }
        }
        
        return retour;
    }
    
    public final int hashCode() {
        int code = 0;
        
        if (this.getCdEFisc() != null) {
            code = this.getCdEFisc().hashCode();
        }
        
        if (this.getNoDistinctionEf() != null) {
            code += +this.getNoDistinctionEf().hashCode();
        }
        
        return code;
    }
    
}
