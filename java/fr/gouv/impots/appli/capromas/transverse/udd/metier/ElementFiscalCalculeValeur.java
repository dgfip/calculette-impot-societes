/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse.udd.metier;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ElementFiscalCalculeValeur", propOrder = { "typeCdEFisc", "vlDelta" })
public class ElementFiscalCalculeValeur extends ElementFiscalValeur {
    
    public ElementFiscalCalculeValeur() {
        super();
    }
    
    public ElementFiscalCalculeValeur(final ElementFiscalValeur elementFiscalValeur) {
        
        super(elementFiscalValeur);
    }
    
    public ElementFiscalCalculeValeur(final String newCdEFisc, final Integer newNoDistinctionEf, final Double newVlEFisc, final Double newVlDelta) {
        super(newCdEFisc, newNoDistinctionEf, newVlEFisc);
        
        this.vlDelta = newVlDelta;
    }
    
    public ElementFiscalCalculeValeur(final String newCdEFisc, final Integer newNoDistinctionEf, final Double newVlEFisc) {
        super(newCdEFisc, newNoDistinctionEf, newVlEFisc);
    }
    
    public ElementFiscalCalculeValeur(final String newCdEFisc, final Double newVlEFisc) {
        super(newCdEFisc, newVlEFisc);
    }
    
    @XmlElement(required = true, nillable = true)
    protected String typeCdEFisc;
    @XmlElement(required = true, type = Double.class, nillable = true)
    protected Double vlDelta;
    
    public String getTypeCdEFisc() {
        return typeCdEFisc;
    }
    
    public void setTypeCdEFisc(String value) {
        this.typeCdEFisc = value;
    }
    
    public Double getVlDelta() {
        return vlDelta;
    }
    
    public void setVlDelta(Double value) {
        this.vlDelta = value;
    }
    
}
