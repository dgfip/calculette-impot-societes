/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse.udd.anomalies;

public final class AnomalieF030Valeur extends AnomalieValeur {
    
    private String refElF;
    
    private String defElF;
    
    private Integer noDistinction;
    
    public AnomalieF030Valeur() {
        super(CODE_ANO_F030);
    }
    
    public void setRefElF(String newRefElF) {
        this.refElF = newRefElF;
    }
    
    public String getRefElF() {
        return this.refElF;
    }
    
    public void setDefElF(String newDefElF) {
        this.defElF = newDefElF;
    }
    
    public String getDefElF() {
        return this.defElF;
    }
    
    public void setNoDistinction(Integer newNoDistinction) {
        this.noDistinction = newNoDistinction;
    }
    
    public Integer getNoDistinction() {
        return this.noDistinction;
    }
}
