/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse.udd.erreurs;

import fr.gouv.impots.appli.capromas.transverse.udd.anomalies.AnomalieF015Valeur;

public final class F015Erreur extends ConteneurAnomalieErreur {
    
    private AnomalieF015Valeur[] anomaliesF015;
    
    public F015Erreur() {
        super();
    }
    
    public void setAnomaliesF015(AnomalieF015Valeur[] newAnomaliesF015) {
        this.anomaliesF015 = newAnomaliesF015;
    }
    
    public AnomalieF015Valeur[] getAnomaliesF015() {
        return this.anomaliesF015;
    }
    
    public void setAnomaliesF015(int index, AnomalieF015Valeur newAnomalieF015) {
        this.anomaliesF015[index] = newAnomalieF015;
    }
    
    public AnomalieF015Valeur getAnomaliesF015(int index) {
        return this.anomaliesF015[index];
    }
}
