/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse.udd.anomalies;

import java.util.Calendar;

public final class AnomalieF195Valeur extends AnomalieValeur {
    
    private Calendar dtFisc;
    
    private Calendar dtDebPerFisc;
    
    private Calendar dtFinPerFisc;
    
    private String refElFBase;
    
    private String defElFBase;
    
    private String refElFResultat;
    
    private String defElFResultat;
    
    private String cdBareme;
    
    private String lbBareme;
    
    public AnomalieF195Valeur() {
        super(CODE_ANO_F195);
    }
    
    public void setDtFisc(Calendar newDtFisc) {
        this.dtFisc = newDtFisc;
    }
    
    public Calendar getDtFisc() {
        return this.dtFisc;
    }
    
    public void setDtDebPerFisc(Calendar newDtDebPerFisc) {
        this.dtDebPerFisc = newDtDebPerFisc;
    }
    
    public Calendar getDtDebPerFisc() {
        return this.dtDebPerFisc;
    }
    
    public void setDtFinPerFisc(Calendar newDtFinPerFisc) {
        this.dtFinPerFisc = newDtFinPerFisc;
    }
    
    public Calendar getDtFinPerFisc() {
        return this.dtFinPerFisc;
    }
    
    public void setRefElFBase(String newRefElFBase) {
        this.refElFBase = newRefElFBase;
    }
    
    public String getRefElFBase() {
        return this.refElFBase;
    }
    
    public void setDefElFBase(String newDefElfBase) {
        this.defElFBase = newDefElfBase;
    }
    
    public String getDefElFBase() {
        return this.defElFBase;
    }
    
    public void setRefElFResultat(String newRefElFResultat) {
        this.refElFResultat = newRefElFResultat;
    }
    
    public String getRefElFResultat() {
        return this.refElFResultat;
    }
    
    public void setDefElFResultat(String newDefElFResultat) {
        this.defElFResultat = newDefElFResultat;
    }
    
    public String getDefElFResultat() {
        return this.defElFResultat;
    }
    
    public void setCdBareme(String newCdBareme) {
        this.cdBareme = newCdBareme;
    }
    
    public String getCdBareme() {
        return this.cdBareme;
    }
    
    public void setLbBareme(String newLbBareme) {
        this.lbBareme = newLbBareme;
    }
    
    public String getLbBareme() {
        return this.lbBareme;
    }
}
