/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse.udd.erreurs;

import fr.gouv.impots.appli.capromas.transverse.udd.anomalies.AnomalieF020Valeur;

public final class F020Erreur extends ConteneurAnomalieErreur {
    
    private AnomalieF020Valeur[] anomaliesF020;
    
    public F020Erreur() {
        super();
    }
    
    public void setAnomaliesF020(AnomalieF020Valeur[] newAnomaliesF020) {
        this.anomaliesF020 = newAnomaliesF020;
    }
    
    public AnomalieF020Valeur[] getAnomaliesF020() {
        return this.anomaliesF020;
    }
    
    public void setAnomaliesF020(int index, AnomalieF020Valeur newAnomalieF020) {
        this.anomaliesF020[index] = newAnomalieF020;
    }
    
    public AnomalieF020Valeur getAnomaliesF020(int index) {
        return this.anomaliesF020[index];
    }
}
