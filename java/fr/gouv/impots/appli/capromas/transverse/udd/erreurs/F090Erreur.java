/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse.udd.erreurs;

import fr.gouv.impots.appli.capromas.transverse.udd.anomalies.AnomalieF090Valeur;

public final class F090Erreur extends ConteneurAnomalieErreur {
    
    private AnomalieF090Valeur[] anomaliesF090;
    
    public F090Erreur() {
        super();
    }
    
    public void setAnomaliesF090(AnomalieF090Valeur[] newAnomaliesF090Valeur) {
        this.anomaliesF090 = newAnomaliesF090Valeur;
    }
    
    public AnomalieF090Valeur[] getAnomaliesF090() {
        return this.anomaliesF090;
    }
    
    public void setAnomaliesF090(int index, AnomalieF090Valeur newAnomalieF090Valeur) {
        this.anomaliesF090[index] = newAnomalieF090Valeur;
    }
    
    public AnomalieF090Valeur getAnomaliesF090(int index) {
        return this.anomaliesF090[index];
    }
}
