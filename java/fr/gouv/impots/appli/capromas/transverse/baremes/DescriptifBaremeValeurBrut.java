/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse.baremes;

import java.util.Calendar;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlRootElement(name = "bareme")
public final class DescriptifBaremeValeurBrut {
    
    private String cdBareme;
    
    private String lbBareme;
    
    private Calendar dtFinVldtBareme;
    
    private String typeUtilite;
    
    private String onUnitaire;
    
    private String onCumulatif;
    
    private String onBorneInclu;
    
    private String onBorneMini;
    
    private String onAnnuel;
    
    private String seuilPlafond;
    
    public DescriptifBaremeValeurBrut() {
        super();
    }
    
    public void setCdBareme(String newCdBareme) {
        this.cdBareme = newCdBareme;
    }
    
    @XmlElement(name = "CdBareme")
    public String getCdBareme() {
        return this.cdBareme;
    }
    
    public void setLbBareme(String newLbBareme) {
        this.lbBareme = newLbBareme;
    }
    
    @XmlElement(name = "LbBareme")
    public String getLbBareme() {
        return this.lbBareme;
    }
    
    public void setDtFinVldtBareme(Calendar newDtFinVldtBareme) {
        this.dtFinVldtBareme = newDtFinVldtBareme;
    }
    
    @XmlElement(name = "DtFinVldtBareme")
    @XmlJavaTypeAdapter(fr.gouv.impots.appli.capromas.transverse.serialisation.CalendarDateFinJaxbAdapter.class)
    public Calendar getDtFinVldtBareme() {
        return this.dtFinVldtBareme;
    }
    
    public void setTypeUtilite(String newTypeUtilite) {
        this.typeUtilite = newTypeUtilite;
    }
    
    @XmlElement(name = "TypeUtilite")
    public String getTypeUtilite() {
        return this.typeUtilite;
    }
    
    public void setOnUnitaire(String newOnUnitaire) {
        this.onUnitaire = newOnUnitaire;
    }
    
    @XmlElement(name = "OnUnitaire")
    public String getOnUnitaire() {
        return this.onUnitaire;
    }
    
    public void setOnCumulatif(String newOnCumulatif) {
        this.onCumulatif = newOnCumulatif;
    }
    
    @XmlElement(name = "OnCumulatif")
    public String getOnCumulatif() {
        return this.onCumulatif;
    }
    
    public void setOnBorneInclu(String newOnBorneInclu) {
        this.onBorneInclu = newOnBorneInclu;
    }
    
    @XmlElement(name = "OnBorneInclu")
    public String getOnBorneInclu() {
        return this.onBorneInclu;
    }
    
    public void setOnBorneMini(String newOnBorneMini) {
        this.onBorneMini = newOnBorneMini;
    }
    
    @XmlElement(name = "OnBorneMini")
    public String getOnBorneMini() {
        return this.onBorneMini;
    }
    
    public void setOnAnnuel(String newOnAnnuel) {
        this.onAnnuel = newOnAnnuel;
    }
    
    @XmlElement(name = "OnAnnuel")
    public String getOnAnnuel() {
        return this.onAnnuel;
    }
    
    public void setSeuilPlafond(String newSeuilPlafond) {
        this.seuilPlafond = newSeuilPlafond;
    }
    
    @XmlElement(name = "SeuilPlafond")
    public String getSeuilPlafond() {
        return this.seuilPlafond;
    }
}
