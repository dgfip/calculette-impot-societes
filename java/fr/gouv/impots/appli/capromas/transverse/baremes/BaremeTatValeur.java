/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse.baremes;

public final class BaremeTatValeur {
    
    private boolean onUnitaire;
    
    private boolean onCumulatif;
    
    private boolean onBorneInclu;
    
    private boolean onBorneMini;
    
    public BaremeTatValeur() {
        super();
    }
    
    public void setOnUnitaire(boolean newOnUnitaire) {
        this.onUnitaire = newOnUnitaire;
    }
    
    public boolean getOnUnitaire() {
        return this.onUnitaire;
    }
    
    public void setOnCumulatif(boolean newOnCumulatif) {
        this.onCumulatif = newOnCumulatif;
    }
    
    public boolean getOnCumulatif() {
        return this.onCumulatif;
    }
    
    public void setOnBorneInclu(boolean newOnBorneInclu) {
        this.onBorneInclu = newOnBorneInclu;
    }
    
    public boolean getOnBorneInclu() {
        return this.onBorneInclu;
    }
    
    public void setOnBorneMini(boolean newOnBorneMini) {
        this.onBorneMini = newOnBorneMini;
    }
    
    public boolean getOnBorneMini() {
        return this.onBorneMini;
    }
    
}
