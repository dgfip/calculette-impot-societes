/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse.baremes;

import java.util.Calendar;

public final class DescriptifBaremeValeur {
    
    private String cdBareme;
    
    private String lbBareme;
    
    private boolean onGeneral;
    
    private String typeUtilite;
    
    private Calendar dtFinVldtBareme;
    
    private BaremeGeneralValeur baremeGeneral;
    
    private BaremeAttributaireValeur baremeAttributaire;
    
    public DescriptifBaremeValeur() {
        super();
    }
    
    public void setCdBareme(String newCdBareme) {
        this.cdBareme = newCdBareme;
    }
    
    public String getCdBareme() {
        return this.cdBareme;
    }
    
    public void setLbBareme(String newLbBareme) {
        this.lbBareme = newLbBareme;
    }
    
    public String getLbBareme() {
        return this.lbBareme;
    }
    
    public void setOnGeneral(boolean newOnGeneral) {
        this.onGeneral = newOnGeneral;
    }
    
    public boolean isOnGeneral() {
        return this.onGeneral;
    }
    
    public void setTypeUtilite(String newTypeUtilite) {
        this.typeUtilite = newTypeUtilite;
    }
    
    public String getTypeUtilite() {
        return this.typeUtilite;
    }
    
    public void setDtFinVldtBareme(Calendar newDtFinVldtBareme) {
        this.dtFinVldtBareme = newDtFinVldtBareme;
    }
    
    public Calendar getDtFinVldtBareme() {
        return this.dtFinVldtBareme;
    }
    
    public void setBaremeGeneral(BaremeGeneralValeur newTheBaremeGeneralValeur) {
        this.baremeGeneral = newTheBaremeGeneralValeur;
    }
    
    public BaremeGeneralValeur getBaremeGeneral() {
        return this.baremeGeneral;
    }
    
    public void setBaremeAttributaire(BaremeAttributaireValeur newTheBaremeAttributaireValeur) {
        this.baremeAttributaire = newTheBaremeAttributaireValeur;
    }
    
    public BaremeAttributaireValeur getBaremeAttributaire() {
        return this.baremeAttributaire;
    }
    
}
