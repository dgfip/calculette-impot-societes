/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse.baremes;

import java.util.Calendar;

public final class BaremeAttributaireValeur {
    
    private String cdBaremeRef;
    
    private String cdAttributaire;
    
    private Calendar debVldtBaremeAttrib;
    
    private Calendar finVldtBaremeAttrib;
    
    private boolean onClefRepartition;
    
    private DescriptifBaremeValeur baremeRef;
    
    public BaremeAttributaireValeur() {
        super();
    }
    
    public void setCdBaremeRef(String newCdBaremeRef) {
        this.cdBaremeRef = newCdBaremeRef;
    }
    
    public String getCdBaremeRef() {
        return this.cdBaremeRef;
    }
    
    public void setCdAttributaire(String newCdAttributaire) {
        this.cdAttributaire = newCdAttributaire;
    }
    
    public String getCdAttributaire() {
        return this.cdAttributaire;
    }
    
    public void setDebVldtBaremeAttrib(Calendar newDebVldtBaremeAttrib) {
        this.debVldtBaremeAttrib = newDebVldtBaremeAttrib;
    }
    
    public Calendar getDebVldtBaremeAttrib() {
        return this.debVldtBaremeAttrib;
    }
    
    public void setFinVldtBaremeAttrib(Calendar newFinVldtBaremeAttrib) {
        this.finVldtBaremeAttrib = newFinVldtBaremeAttrib;
    }
    
    public Calendar getFinVldtBaremeAttrib() {
        return this.finVldtBaremeAttrib;
    }
    
    public void setOnClefRepartition(boolean newOnClefRepartition) {
        this.onClefRepartition = newOnClefRepartition;
    }
    
    public boolean isOnClefRepartition() {
        return this.onClefRepartition;
    }
    
    public void setBaremeRef(DescriptifBaremeValeur newBaremeRef) {
        this.baremeRef = newBaremeRef;
    }
    
    public DescriptifBaremeValeur getBaremeRef() {
        return this.baremeRef;
    }
    
}
