/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse.baremes;

import java.util.Calendar;

import fr.gouv.impots.appli.capromas.transverse.ValeursBaremeSansTrancheValeur;

public final class RechercheValeursBaremeReponseValeur {
    
    private String cdBareme;
    
    private String typeUtilite;
    
    private Calendar dtFinVldtBareme;
    
    private ValeursBaremeSansTrancheValeur theValeursBaremeSansTrancheValeur;
    
    public RechercheValeursBaremeReponseValeur() {
        super();
    }
    
    public void setCdBareme(String cdBareme) {
        this.cdBareme = cdBareme;
    }
    
    public String getCdBareme() {
        return this.cdBareme;
    }
    
    public void setTypeUtilite(String typeUtilite) {
        this.typeUtilite = typeUtilite;
    }
    
    public String getTypeUtilite() {
        return this.typeUtilite;
    }
    
    public void setDtFinVldtBareme(Calendar dtFinVldtBareme) {
        this.dtFinVldtBareme = dtFinVldtBareme;
    }
    
    public Calendar getDtFinVldtBareme() {
        return this.dtFinVldtBareme;
    }
    
    public void setTheValeursBaremeSansTrancheValeur(ValeursBaremeSansTrancheValeur theValeursBaremeSansTrancheValeur) {
        this.theValeursBaremeSansTrancheValeur = theValeursBaremeSansTrancheValeur;
    }
    
    public ValeursBaremeSansTrancheValeur getTheValeursBaremeSansTrancheValeur() {
        return this.theValeursBaremeSansTrancheValeur;
    }
    
}