/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse.baremes;

import java.util.Calendar;

public final class RechercheValeursBaremeValeur {
    
    private String cdBareme;
    
    private Calendar dateLeg1;
    
    private Calendar dateLeg2;
    
    private Double montant;
    
    public RechercheValeursBaremeValeur() {
        super();
    }
    
    public void setCdBareme(String newCdBareme) {
        this.cdBareme = newCdBareme;
    }
    
    public String getCdBareme() {
        return this.cdBareme;
    }
    
    public void setDateLeg1(Calendar newDateLeg1) {
        this.dateLeg1 = newDateLeg1;
    }
    
    public Calendar getDateLeg1() {
        return this.dateLeg1;
    }
    
    public void setDateLeg2(Calendar newDateLeg2) {
        this.dateLeg2 = newDateLeg2;
    }
    
    public Calendar getDateLeg2() {
        return this.dateLeg2;
    }
    
    public void setMontant(Double newMontant) {
        this.montant = newMontant;
    }
    
    public Double getMontant() {
        return this.montant;
    }
    
}
