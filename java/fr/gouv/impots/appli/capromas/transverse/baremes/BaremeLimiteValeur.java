/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse.baremes;

public final class BaremeLimiteValeur {
    
    private String seuilPlafond;
    
    private boolean onAnnuel;
    
    public BaremeLimiteValeur() {
        super();
    }
    
    public void setSeuilPlafond(String newSeuilPlafond) {
        this.seuilPlafond = newSeuilPlafond;
    }
    
    public String getSeuilPlafond() {
        return this.seuilPlafond;
    }
    
    public void setOnAnnuel(boolean newOnAnnuel) {
        this.onAnnuel = newOnAnnuel;
    }
    
    public boolean getOnAnnuel() {
        return this.onAnnuel;
    }
    
}
