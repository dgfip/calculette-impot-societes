/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse.baremes;

public final class BaremeGeneralValeur {
    
    private boolean onIndividualise;
    
    private BaremeLimiteValeur baremeLimite;
    
    private BaremeTstValeur baremeTst;
    
    private BaremeTatValeur baremeTat;
    
    public BaremeGeneralValeur() {
        super();
    }
    
    public BaremeGeneralValeur(final boolean newOnIndividualise) {
        this.onIndividualise = newOnIndividualise;
    }
    
    public void setOnIndividualise(final boolean newOnIndividualise) {
        this.onIndividualise = newOnIndividualise;
    }
    
    public boolean getOnIndividualise() {
        return this.onIndividualise;
    }
    
    public void setBaremeLimite(BaremeLimiteValeur newTheBaremeLimiteValeur) {
        this.baremeLimite = newTheBaremeLimiteValeur;
    }
    
    public BaremeLimiteValeur getBaremeLimite() {
        return this.baremeLimite;
    }
    
    public void setBaremeTst(BaremeTstValeur newTheBaremeTstValeur) {
        this.baremeTst = newTheBaremeTstValeur;
    }
    
    public BaremeTstValeur getBaremeTst() {
        return this.baremeTst;
    }
    
    public void setBaremeTat(BaremeTatValeur newTheBaremeTatValeur) {
        this.baremeTat = newTheBaremeTatValeur;
    }
    
    public BaremeTatValeur getBaremeTat() {
        return this.baremeTat;
    }
    
}
