/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse.baremes;

import java.util.Arrays;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "baremes")
public final class DescriptifBaremeValeurListe {
    
    private DescriptifBaremeValeurBrut[] tabDescriptifsBaremes;
    
    public DescriptifBaremeValeurListe() {
        super();
    }
    
    @XmlElement(name = "bareme")
    public DescriptifBaremeValeurBrut[] getTabDescriptifsBaremes() {
        return tabDescriptifsBaremes;
    }
    
    public void setTabDescriptifsBaremes(DescriptifBaremeValeurBrut[] tabDescriptifsBaremes) {
        this.tabDescriptifsBaremes = tabDescriptifsBaremes;
    }
    
    public void setDescriptifsBaremes(List<DescriptifBaremeValeurBrut> newDescriptifsBaremes) {
        this.tabDescriptifsBaremes = new DescriptifBaremeValeurBrut[newDescriptifsBaremes.size()];
        newDescriptifsBaremes.toArray(this.tabDescriptifsBaremes);
    }
    
    public List<DescriptifBaremeValeurBrut> getDescriptifsBaremes() {
        return Arrays.asList(this.tabDescriptifsBaremes);
    }
    
}
