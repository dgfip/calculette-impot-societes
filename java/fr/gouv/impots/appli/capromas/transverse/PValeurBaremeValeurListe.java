/*
 * Copyright or © or Copr.[DGFIP][2019]
 * 
 * Ce logiciel  a été  initialement  développé  par  la  Direction  Générale des
 * Finances Publiques pour permettre le calcul de l'impôt sur les sociétés 2019.
 * 
 * Ce logiciel est régi  par la licence CeCILL 2.1  soumise au droit français et
 * respectant  les principes  de diffusion  des  logiciels  libres.  Vous pouvez
 * utiliser, modifier et/ou redistribuer  ce programme sous les conditions de la
 * licence CeCILL 2.1  telle que diffusée  par le CEA, le CNRS et l'INRIA sur le
 * site "http://www.cecill.info".
 * 
 * Le fait que  vous puissiez accéder  à cet en-tête signifie que vous avez pris
 * connaissance de la licence CeCILL 2.1 et que vous en avez accepté les termes.
 */

package fr.gouv.impots.appli.capromas.transverse;

import java.util.Arrays;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "valeurs_baremes")
public final class PValeurBaremeValeurListe {
    
    private PValeurBaremeValeur[] tabValeurs;
    
    public PValeurBaremeValeurListe() {
        super();
    }
    
    @XmlElement(name = "valeur_bareme")
    public PValeurBaremeValeur[] getTabValeurs() {
        return tabValeurs;
    }
    
    public void setTabValeurs(PValeurBaremeValeur[] tabValeurs) {
        this.tabValeurs = tabValeurs;
    }
    
    public void setValeurs(List<PValeurBaremeValeur> newValeurs) {
        this.tabValeurs = new PValeurBaremeValeur[newValeurs.size()];
        newValeurs.toArray(this.tabValeurs);
    }
    
    public List<PValeurBaremeValeur> getValeurs() {
        return Arrays.asList(this.tabValeurs);
    }
    
}